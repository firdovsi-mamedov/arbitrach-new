function plusMinus(elem) {
    if (jQuery(elem).find("i").attr('class') == 'fa fa-plus') {
        jQuery(elem).find("i").toggleClass('fa fa-plus fa fa-times')
    } else if (jQuery(elem).find("i").attr('class') == 'fa fa-times') {
        jQuery(elem).find("i").toggleClass('fa fa-times fa fa-plus')
    }
}

function tagsSearch(elem) {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/tags/getTag",
        type: "POST",
        data: {tag: elem.value},
        success: function (data) {
            if (elem.value != "") {
                $("#TEstRes").append(data);
            }else{
                $("#TEstRes").html('');
            }
        },
        error: function (ts) {
            alert(ts.responseText)
        }
    });
}

