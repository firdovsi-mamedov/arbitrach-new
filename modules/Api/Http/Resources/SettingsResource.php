<?php

namespace Modules\Api\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Modules\Post\Http\Resources\PostResource;

class SettingsResource extends Resource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'show_posts_with_rating_higher'    => $this->show_posts_with_rating_higher,
            'posts_per_page'                   => $this->posts_per_page,
            'is_paginated'                     => $this->is_paginated,
            'is_content_collapsed'             => $this->is_content_collapsed,
            'is_show_all_long_post'            => $this->is_show_all_long_post,
            'is_tags_on_top'                   => $this->is_tags_on_top,
            'is_gif_paused_on_posts'           => $this->is_gif_paused_on_posts,
            'is_show_same_posts'               => $this->is_show_same_posts,
            'show_comments_with_rating_higher' => $this->show_comments_with_rating_higher,
            'collapse_comment_after_level'     => $this->collapse_comment_after_level,
            'is_gif_paused_on_comments'        => $this->is_gif_paused_on_comments,
            'is_show_scroll_top'               => $this->is_show_scroll_top,
            'is_settings_on_menu'              => $this->is_settings_on_menu,
            'is_disable_ads'                   => $this->is_disable_ads,
            'is_night_mode'                    => $this->is_night_mode
        ];
    }
}
