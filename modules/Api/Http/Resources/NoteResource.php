<?php

namespace Modules\Api\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Modules\Post\Http\Resources\PostResource;
use Modules\Users\Http\Resources\UserResource;

class NoteResource extends Resource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'user_id'     => $this->user_id,
            'notifier_id' => $this->notifier_id,
            'text'        => $this->text,
            'user'        => $this->whenLoaded('user', function () {
                return UserResource::make($this->user);
            })
        ];
    }
}
