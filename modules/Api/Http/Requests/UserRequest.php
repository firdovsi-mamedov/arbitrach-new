<?php

namespace Modules\Api\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Users\Entities\User;

class UserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'show_posts_with_rating_higher'=> 'sometimes|integer',
                'posts_per_page'=> 'sometimes|integer',
                'is_paginated'=> 'sometimes|boolean',
                'is_content_collapsed'=> 'sometimes|boolean',
                'is_show_all_long_post'=> 'sometimes|boolean',
                'is_tags_on_top'=> 'sometimes|boolean',
                'is_gif_paused_on_posts'=> 'sometimes|boolean',
                'is_show_same_posts'=> 'sometimes|boolean',
                'show_comments_with_rating_higher'=> 'sometimes|boolean',
                'collapse_comment_after_level'=> 'sometimes|integer',
                'is_gif_paused_on_comments'=> 'sometimes|boolean',
                'is_show_scroll_top'=> 'sometimes|boolean',
                'is_settings_on_menu'=> 'sometimes|boolean',
                'is_disable_ads'=> 'sometimes|boolean',
                'is_night_mode'=> 'sometimes|boolean'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
