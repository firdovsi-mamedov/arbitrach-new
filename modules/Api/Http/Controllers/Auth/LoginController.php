<?php

namespace Modules\Api\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Modules\Users\Http\Requests\LoginRequest;

class LoginController extends Controller
{

    public function login(LoginRequest $request)
    {
        $result = \Auth::attempt([
            'email'    => $request->email,
            'password' => $request->password
        ]);

        return response()->json(['success' => $result]);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
