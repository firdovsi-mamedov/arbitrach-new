<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Post\Entities\Post;
use Modules\Post\Http\Resources\PostResource;
use Modules\Users\Entities\Community;
use Modules\Users\Http\Resources\CommunityResource;

class CommunityController extends ApiController
{

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getPosts()
    {
        $posts = Post::prepared()->whereHas('community')->paginate($this->paginate);

        return PostResource::collection($posts);
    }

    /**
     * @param Request $request
     * @param Community $community
     * @param null $filter
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function show(Request $request, Community $community, $filter = null)
    {
        $posts = $community->posts()->prepared();

        switch ($filter) {
            case 'best' :
                $posts = $posts->orderByDesc('rating');
                break;
            case 'new' :
                $posts = $posts->latest();
                break;
            case 'hot' :
                break;
            case 'search' :
                if ($filter = $request->get('filter', false)) {
                    if ($search = array_get($filter, 'search', false)) {
                        $posts = $posts->where('title', 'like', "%$search%");
                    }

                    if ($rating = array_get($filter, 'rating', false)) {
                        $posts = $posts->where('rating', '>=', $rating);
                    }

                    if ($tag = array_get($filter, 'tag', false)) {
                        $posts = $posts->whereHas('tags', function ($q) use ($tag) {
                            $q->where('tag_id', $tag['id']);
                        });
                    }
                }
                break;
            default:
                break;
        }

        $posts = $posts->paginate($this->paginate);

        return PostResource::collection($posts);
    }

    /**
     * @param Request $request
     * @param Community $community
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function postsFilter(Request $request, Community $community)
    {
        $filter = $request->get('filter', []);
        $posts  = $community->posts()->prepared();

        if ($search = $filter['search']) {
            $posts = $posts->where('title', 'like', "%$search%");
        }

        if ($rating = $filter['rating']) {
            $posts = $posts->where('rating', '>=', $rating);
        }

        if ($tag = $filter['tag']) {
            $posts = $posts->whereHas('tags', function ($q) use ($tag) {
                $q->where('tag_id', $tag['id']);
            });
        }

        $posts = $posts->paginate($this->paginate);

        return PostResource::collection($posts);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function subscribe()
    {
        \Auth::user()->communities()->attach(request('id', false), ['ignored' => false]);

        return response()->json();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function unsubscribe()
    {
        \Auth::user()->communities()->detach(request('id', false));

        return response()->json();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ignore()
    {
        \Auth::user()->communities()->attach(request('id'), ['ignored' => true]);

        return response()->json();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function unignore()
    {
        \Auth::user()->communities()->detach(request('id'));

        return response()->json();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSubscribedCommunities()
    {
        $subs_communities = \Auth::user()->communities()->where('ignored', false)->get();
        $communities      = Community::whereNotIn('id', $subs_communities->pluck('id'))->get();

        $subs_communities = CommunityResource::collection($subs_communities)->jsonSerialize();
        $communities      = CommunityResource::collection($communities)->jsonSerialize();

        return response()->json(compact('subs_communities', 'communities'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getIgnoredCommunities()
    {
        $ignored_communities = \Auth::user()->communities()->where('ignored', true)->get();
        $communities         = Community::whereNotIn('id', $ignored_communities->pluck('id'))->get();

        $ignored_communities = CommunityResource::collection($ignored_communities)->jsonSerialize();
        $communities         = CommunityResource::collection($communities)->jsonSerialize();

        return response()->json(compact('ignored_communities', 'communities'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function search(Request $request)
    {
        $communities = Community::select('*');

        if ($search = $request->get('search', false)) {
            $communities = $communities->where('name', 'like', "%$search%");
        }

        $communities = $communities->get();

        return CommunityResource::collection($communities);
    }
}
