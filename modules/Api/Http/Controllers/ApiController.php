<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class ApiController extends Controller
{

    /**
     * @var int
     */
    protected $paginate = 10;

    /**
     * ApiController constructor.
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->paginate = \Auth::user()->posts_per_page ?? 10;
            return $next($request);
        });
    }
}
