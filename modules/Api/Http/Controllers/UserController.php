<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Post\Http\Resources\PostResource;
use Modules\Api\Http\Resources\SettingsResource;
use Modules\Users\Entities\User;

class UserController extends ApiController
{

    /**
     * @param User $user
     * @param string $sort
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function show(User $user, $sort = 'time', Request $request)
    {
        $posts = $user->posts()->prepared();

        switch ($sort) {
            case 'time':
                $posts = $posts->orderBy('created_at');
                break;
            case 'rating':
                $posts = $posts->orderByDesc('rating');
                break;
            case 'my':
                $posts = $posts->whereMy(1);
                break;
            case 'filter':
                if ($filter = $request->get('filter', false)) {
                    if ($search = array_get($filter, 'search', false)) {
                        $posts = $posts->where('title', 'like', "%$search%");
                    }

                    if ($rating = array_get($filter, 'rating', false)) {
                        $posts = $posts->where('rating', '>=', $rating);
                    }

                    if ($tag = array_get($filter, 'tag', false)) {
                        $posts = $posts->whereHas('tags', function ($q) use ($tag) {
                            $q->where('tag_id', $tag['id']);
                        });
                    }
                }
                break;
        }

        $posts = $posts->paginate($this->paginate);

        return PostResource::collection($posts);
    }

    /**
     * @param string $sort
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function posts($sort = 'time', Request $request)
    {
        $posts = \Auth::user()->posts()->prepared();

        switch ($sort) {
            case 'time':
                $posts = $posts->orderBy('created_at');
                break;
            case 'rating':
                $posts = $posts->orderByDesc('rating');
                break;
            case 'my':
                $posts = $posts->whereMy(1);
                break;
            case 'filter':
                if ($filter = $request->get('filter', false)) {
                    if ($search = array_get($filter, 'search', false)) {
                        $posts = $posts->where('title', 'like', "%$search%");
                    }

                    if ($rating = array_get($filter, 'rating', false)) {
                        $posts = $posts->where('rating', '>=', $rating);
                    }

                    if ($tag = array_get($filter, 'tag', false)) {
                        $posts = $posts->whereHas('tags', function ($q) use ($tag) {
                            $q->where('tag_id', $tag['id']);
                        });
                    }
                }
                break;
        }

        $posts = $posts->paginate($this->paginate);

        return PostResource::collection($posts);
    }

    public function getUser()
    {
        return SettingsResource::make(\Auth::user());
    }

    public function changeUser(Request $request)
    {
        $user = User::find(\Auth::id());

        if ($request->elem == 1) {
            $elem = 1;
        } elseif (! $request->elem) {
            $elem = 0;
        } elseif ($request->elem > 1) {
            $elem = $request->elem;
        } else {
            $elem = $request->elem;
        }

        $user->update([$request->property => $elem]);

        return response()->json(['success' => true]);
    }

    /**
     * @param $type
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPostsState($type)
    {
        $state = \Auth::user()->getAttribute("{$type}_posts_state");

        return response()->json(compact('state'));
    }

    /**
     * @param Request $request
     * @param $type
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePostsState(Request $request, $type)
    {
        $state  = $request->get('state', 'show');
        $result = \Auth::user()->update(["{$type}_posts_state" => $state]);

        return response()->json(['success' => $result]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postViewed(Request $request)
    {
        if (! \Auth::check()) {
            return response()->json(['success' => false]);
        }

        if ($post_id = $request->get('post_id', false)) {
            \Auth::user()->viewedPosts()->syncWithoutDetaching($post_id);
        }

        return response()->json(['success' => true]);
    }
}
