<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Api\Http\Resources\NoteResource;
use Modules\Users\Entities\Note;
use Modules\Users\Entities\User;

class NoteController extends Controller
{

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $notes = \Auth::user()->notifiers()->with('user')->get();

        return NoteResource::collection($notes);
    }

    /**
     * @param User $user
     * @return NoteResource
     */
    public function user(User $user)
    {
        $note = $user->notes()->where('notifier_id', \Auth::id())->firstOrFail();

        return NoteResource::make($note);
    }

    /**
     * @param Note $note
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Note $note)
    {
        $result = $note->delete();

        return response()->json(['success' => $result]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'text' => 'required|string|max:255',
            'id'   => 'required|integer|exists:users,id'
        ]);

        $note = Note::where([
            ['notifier_id', '=', \Auth::id()],
            ['user_id', '=', $request->get('id')]
        ])->first();

        if ($validator->passes()) {
            if ($note) {
                $result = $note->update($request->all());
            } else {
                $result = \Auth::user()->notifiers()->create([
                    'text'    => $request->text,
                    'user_id' => $request->id
                ]);
            }
            return response()->json(['success' => (bool)$result]);
        } else {
            return response()->json(['success' => false, 'errors' => $validator->errors()]);
        }
    }
}
