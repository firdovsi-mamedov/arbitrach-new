<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Post\Entities\Comment;
use Modules\Post\Entities\Post;
use Modules\Post\Http\Requests\StoreCommentRequest;
use Modules\Post\Http\Requests\UpdateCommentRequest;
use Modules\Post\Http\Resources\CommentResource;

class CommentController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function imagePreview(Request $request)
    {
        $image = false;

        if ($file = $request->file('image')) {
            $image = \Image::make($file)->resize(92, 92, function ($constraint) {
                $constraint->aspectRatio();
            })->encode('data-url');
        }

        if ($old = $request->get('image', false)) {
            $old   = str_replace_first('/storage', 'public', $old);
            $image = \Image::make(\Storage::path($old))->resize(92, 92, function ($constraint) {
                $constraint->aspectRatio();
            })->encode('data-url');
        }

        return response()->json($image);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getAnswers(Request $request)
    {
        $mode     = $request->get('mode', 'all');
        $comments = Comment::with(['child', 'user', 'post:id,title']);

        if ($filter = $request->get('filter', false)) {
            if ($search = array_get($filter, 'search', false)) {
                $comments = $comments->where('id', 'like', "%$search%");
            }
        }

        if ($mode === 'all') {
            $comments = $comments->where(function ($q) {
                $q->whereIn('post_id', \Auth::user()->posts()->pluck('id'))
                    ->where('parent_id', null)
                    ->orWhere(function ($q) {
                        $q->where('user_id', \Auth::id())
                            ->whereHas('child');
                    });
            });
        }

        if ($mode === 'posts') {
            $comments = $comments->where(function ($q) {
                $q->whereIn('post_id', \Auth::user()->posts()->pluck('id'))
                    ->where('parent_id', null);
            });
        }

        if ($mode === 'comments') {
            $comments = $comments->where(function ($q) {
                $q->where('user_id', \Auth::id())
                    ->whereHas('child');
            });
        }

        $comments = $comments->paginate(10);

        return CommentResource::collection($comments);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function myComments(Request $request)
    {
        $comments = \Auth::user()->comments()->with(['child', 'user', 'post:id,title']);
        $sort     = $request->get('sort', 'date');

        if ($filter = $request->get('filter', false)) {
            if ($search = array_get($filter, 'search', false)) {
                $comments = $comments->where('text', 'like', "%$search%");
            }
        }

        if ($sort === 'date') {
            $comments = $comments->orderByDesc('created_at');
        }

        if ($sort === 'rating') {
            $comments = $comments->orderByDesc('rating');
        }

        $comments = $comments->paginate(10);

        return CommentResource::collection($comments);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function postComments(Request $request)
    {
        $post     = Post::withCount('comments')->findOrFail($request->get('post_id'));
        $comments = $post->rootComments()->with(['childs', 'user']);

        if ($search = $request->get('search', false)) {
            $comments = $comments->where('text', 'like', "%$search%");
        }

        $comments = $comments->paginate(10);

        return CommentResource::collection($comments)
            ->additional(['meta' => ['comments_count' => $post->comments_count]]);
    }

    /**
     * @param StoreCommentRequest $request
     * @return CommentResource
     */
    public function store(StoreCommentRequest $request)
    {
        $comment = \Auth::user()->comments()
            ->create($request->except('image'))
            ->loadMissing(['childs', 'user']);

        if ($image = $request->file('image', false)) {
            $filename = str_random(10).'.'.$image->getClientOriginalExtension();
            $path     = 'public/comments/'.$comment->id;
            $resize   = $image->storeAs($path, $filename);

            \Image::make(\Storage::path($resize))->resize(400, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();

            $comment->update(['image' => $resize]);
        }

        return CommentResource::make($comment);
    }

    /**
     * @param Comment $comment
     * @param UpdateCommentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Comment $comment, UpdateCommentRequest $request)
    {
        $update = collect(['text' => $request->text]);

        if ($image = $request->file('image', false)) {
            $filename = str_random(10).'.'.$image->getClientOriginalExtension();
            $path     = 'public/comments/'.$comment->id;
            $resize   = $image->storeAs($path, $filename);

            \Image::make(\Storage::path($resize))->resize(400, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();

            \Storage::delete($comment->imagePath);
            $update->put('image', $resize);
        } else {
            if (is_null($request->get('image'))) {
                $update->put('image', null);
            }
        }

        $comment->update($update->toArray());

        return CommentResource::make($comment);
    }

    /**
     * @param Comment $comment
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Comment $comment)
    {
        \Storage::delete($comment->imagePath);

        $result = $comment->delete();

        return response()->json(['success' => $result]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function search(Request $request)
    {
        $comments = \Auth::user()->comments()->with(['childs', 'user']);
        $filter   = $request->get('filter');

        if ($search = $filter['search']) {
            $comments = $comments->where('text', 'like', "%$search%");
        }

        return CommentResource::collection($comments->get());
    }
}
