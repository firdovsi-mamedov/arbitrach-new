<?php

namespace Modules\Api\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Post\Entities\Post;
use Modules\Post\Http\Resources\PostResource;

class PostController extends ApiController
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $posts = Post::prepared();


        if ($filter = $request->get('filter', false)) {
            if ($id = array_get($filter, 'category', false)) {
                $posts = $this->setCategoryFilter($id, $posts);
            }

            if ($menu = array_get($filter, 'menu', false)) {
                $posts = $this->setMenuFilter($menu, $posts);
            }

            if ($date = array_get($filter, 'date', false)) {
                $posts = $this->setDateFilter($date, $posts);
            }
        }

        $posts = $posts->paginate($this->paginate);

        return PostResource::collection($posts);
    }

    public function my()
    {
        $posts = \Auth::user()->posts()->prepared()->paginate($this->paginate);

        return PostResource::collection($posts);
    }

    /**
     * @param int $id
     * @param Builder $posts
     * @return Builder
     */
    private function setCategoryFilter(int $id, Builder $posts)
    {
        return $posts->where('category_id', $id);
    }

    /**
     * @param string $menu
     * @param Builder $posts
     * @return Builder|\Illuminate\Database\Query\Builder
     */
    private function setMenuFilter(string $menu, Builder $posts)
    {
        switch ($menu) {
            case 'best' :
                return $posts->orderByDesc('rating');
            case 'new' :
                return $posts->latest();
            case 'partners':
                return $posts;
            default:
                return $posts;
        }
    }

    /**
     * @param $date
     * @param Builder $posts
     * @return Builder|\Illuminate\Database\Query\Builder
     */
    private function setDateFilter($date, Builder $posts)
    {
        if ($from = $date['start']) {
            if ($to = $date['end']) {
                return $posts->whereBetween('created_at', [
                    Carbon::parse($from),
                    Carbon::parse($to)
                ]);
            } else {
                return $posts->whereDate('created_at', Carbon::parse($from));
            }
        } else {
            return $posts;
        }
    }
}
