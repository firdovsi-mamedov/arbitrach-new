<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Api\Http\Resources\SearchResource;
use Modules\Post\Entities\Tag;
use Modules\Users\Entities\Community;
use Modules\Users\Entities\User;

class SearchController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function search(Request $request)
    {
        $search      = $request->get('search', false);
        $tags        = Tag::prepared()->withCount('posts');
        $users       = User::select(['id', 'name', 'avatar']);
        $communities = Community::select(['id', 'name', 'slug', 'image']);
        $result      = collect();

        if ($search) {
            $tags        = $tags->where('name', 'like', "%$search%");
            $users       = $users->where('name', 'like', "%$search%");
            $communities = $communities->where('name', 'like', "%$search%");
        }

        $tags        = $tags->take(5)->get();
        $users       = $users->take(5)->get();
        $communities = $communities->take(5)->get();

        if ($tags->isNotEmpty()) {
            $tags->prepend(['type' => 'Tag', 'title' => 'Теги']);
        }

        if ($users->isNotEmpty()) {
            $users->prepend(['type' => 'User', 'title' => 'Пользователи']);
        }

        if ($communities->isNotEmpty()) {
            $communities->prepend(['type' => 'Community', 'title' => 'Сообщества']);
        }

        $result = $result->merge($tags)->merge($users)->merge($communities);

        return SearchResource::collection($result);
    }
}
