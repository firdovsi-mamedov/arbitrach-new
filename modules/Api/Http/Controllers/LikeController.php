<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Api\Http\Resources\LikeResource;
use Modules\Post\Entities\Comment;
use Modules\Post\Entities\Post;

class LikeController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function posts(Request $request)
    {
        $mode   = $request->get('mode');
        $likes  = \Auth::user()->likedPosts()->with('preparedPost');

        if ($mode === 'disliked') {
            $likes = \Auth::user()->dislikedPosts()->with('preparedPost');
        }

        if ($search = $request->get('search', false)) {
            $likes = $likes->whereHas('preparedPost', function ($q) use ($search) {
                $q->where('title', 'like', "%$search%");
            });
        }

        $likes = $likes->paginate(10);

        return LikeResource::collection($likes);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function likePost(Request $request)
    {
        $post = Post::lockForUpdate()->findOrFail($request->get('id'));
        $like = $post->likes()->where('user_id', \Auth::id())->lockForUpdate()->first();

        if ($like) {
            if ($like->vote === 1) {
                return response()->json(['success' => false]);
            }

            if ($like->vote === -1) {
                $like->update(['vote' => 0]);
            } else {
                $like->update(['vote' => 1]);
            }
        } else {
            $post->likes()->create([
                'user_id' => \Auth::id(),
                'vote'    => 1
            ]);
        }

        $post->increment('rating', 1);
        $post->user()->lockForUpdate()->increment('rating', 1);

        return response()->json(['success' => true]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function dislikePost(Request $request)
    {
        $post = Post::lockForUpdate()->findOrFail($request->get('id'));
        $like = $post->likes()->where('user_id', \Auth::id())->lockForUpdate()->first();

        if ($like) {
            if ($like->vote === -1) {
                return response()->json(['success' => false]);
            }

            if ($like->vote === 1) {
                $like->update(['vote' => 0]);
            } else {
                $like->update(['vote' => -1]);
            }
        } else {
            $post->likes()->create([
                'user_id' => \Auth::id(),
                'vote'    => -1
            ]);
        }

        $post->decrement('rating', 1);
        $post->user()->lockForUpdate()->decrement('rating', 1);

        return response()->json(['success' => true]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function likeComment(Request $request)
    {
        $comment = Comment::lockForUpdate()->findOrFail($request->get('id'));
        $like    = $comment->likes()->where('user_id', \Auth::id())->lockForUpdate()->first();

        if ($like) {
            if ($like->vote === 1) {
                return response()->json(['success' => false]);
            }

            if ($like->vote === -1) {
                $like->update(['vote' => 0]);
            } else {
                $like->update(['vote' => 1]);
            }
        } else {
            $comment->likes()->create([
                'user_id' => \Auth::id(),
                'vote'    => 1
            ]);
        }

        $comment->increment('rating', 1);
        $comment->user()->lockForUpdate()->increment('rating', 0.5);

        return response()->json(['success' => true]);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function dislikeComment(Request $request)
    {
        $comment = Comment::lockForUpdate()->findOrFail($request->get('id'));
        $like    = $comment->likes()->where('user_id', \Auth::id())->lockForUpdate()->first();

        if ($like) {
            if ($like->vote === -1) {
                return response()->json(['success' => false]);
            }

            if ($like->vote === 1) {
                $like->update(['vote' => 0]);
            } else {
                $like->update(['vote' => -1]);
            }
        } else {
            $comment->likes()->create([
                'user_id' => \Auth::id(),
                'vote'    => 1
            ]);
        }

        $comment->decrement('rating', 1);
        $comment->user()->lockForUpdate()->decrement('rating', 0.5);

        return response()->json(['success' => true]);
    }
}
