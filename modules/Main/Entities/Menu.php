<?php

namespace Modules\Main\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Modules\Post\Entities\Category;

/**
 * Modules\Main\Entities\Menu
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Post\Entities\Category[] $categories
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Main\Entities\Menu prepared()
 * @mixin \Eloquent
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property int $order
 * @property int $active
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Main\Entities\Menu whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Main\Entities\Menu whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Main\Entities\Menu whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Main\Entities\Menu whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Main\Entities\Menu whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Main\Entities\Menu whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Main\Entities\Menu whereUpdatedAt($value)
 */
class Menu extends Model
{

    protected $fillable = ['title', 'slug', 'order', 'active'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function scopePrepared($query)
    {
        /** @var Builder $query */
        $query->where('active', true)->orderBy('order');
    }
}
