<?php

namespace Modules\Main\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Main\Entities\Menu;
use Modules\Post\Entities\Category;
use Modules\Post\Entities\Post;

class MainController extends Controller
{

    /**
     * @param Menu $menu
     * @param Category $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Menu $menu, Category $category)
    {
        if (! $menu->exists) {
            $menu = Menu::with('categories')->prepared()->firstOrFail();
        }

        $menu->loadMissing('categories');

        $categories = $menu->categories()->prepared()->get();

        return view('post::index', compact('categories', 'menu', 'category'));
    }
}
