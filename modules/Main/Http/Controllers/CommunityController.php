<?php

namespace Modules\Main\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Main\Http\Requests\StoreCommunityRequest;
use Modules\Post\Entities\Post;
use Modules\Users\Entities\Community;
use Modules\Users\Http\Resources\CommunityResource;
use Modules\Post\Entities\Tag;
use Storage;

class CommunityController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('main::communities.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function all()
    {
        $communities = Community::withCount(['posts', 'users'])->with('tags')->get();

        return view('main::communities.all', compact('communities'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function mine()
    {
        $communities = \Auth::user()->communities()
            ->withCount(['posts', 'users'])
            ->with('tags')
            ->get();

        return view('main::communities.mine', compact('communities'));
    }

    /**
     * @param Community $community
     * @param null $filter
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Community $community)
    {
        $community->loadMissing('tags');

        return view('main::communities.show', compact('community'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function filter(Request $request)
    {
        $filter      = $request->get('filter', []);
        $communities = $request->get('mode') === 'all' ? Community::query() : \Auth::user()->communities();
        $communities = $communities->withCount(['posts', 'users'])->with('tags');

        if ($search = $filter['search']) {
            $communities = $communities->where('name', 'like', "%$search%");
        }

        if ($type = $filter['type']) {
            $communities = $communities->where('private', $type === 'private' ? true : false);
        }

        if ($tag = $filter['tag']) {
            $communities = $communities->whereHas('tags', function ($q) use ($tag) {
                $q->where('tag_id', $tag['id']);
            });
        }

        $communities = $communities->get();

        return view('main::communities.partials.list', compact('communities'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('main::communities.create');
    }

    /**
     * @param StoreCommunityRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreCommunityRequest $request)
    {
        $community = \Auth::user()->community()->create($request->except(['image', 'tags']));

        if ($image = $request->file('image', false)) {
            $filename = str_random(10).'.'.$image->getClientOriginalExtension();
            $path     = $image->storeAs('public/communities/'.\Auth::id(), $filename);

            $community->update(['image' => $path]);
        } else {
            $default = 'public/communities/default_image.svg';

            $community->update(['image' => $default]);
        }

        if ($tags = $request->get('tags', false)) {
            if (! empty($tags)) {
                $community->tags()->sync(collect($tags)->pluck('id'));
            }
        }

        return response()->json(['url' => route('communities.show', $community->slug)]);
    }
}
