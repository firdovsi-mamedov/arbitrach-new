<?php

namespace Modules\Main\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class StoreCommunityRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'    => 'required|string|max:255',
            'slug'    => 'required|string|max:255|unique:communities,slug',
            'image'   => 'nullable|image',
            'private' => 'required',
            'tags'    => 'nullable|array'
        ];
    }

    protected function prepareForValidation()
    {
        if ($name = $this->get('name', false)) {
            $this->merge(['slug' => Str::slug($this->name)]);
        }

        if ($tags = $this->get('tags', false)) {
            $this->merge(['tags' => json_decode($this->tags)]);
        }

        if ($private = $this->get('private', false)) {
            $this->merge(['private' => $private === 'true' ? true : false]);
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
