<!doctype html>

<html lang="ru">
<head>
    <meta charset="utf-8">

    <title>Arbitrach</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @stack('css')
</head>

<body @if(auth()->check() && auth()->user()->is_night_mode) class="night" @endif>
@if(auth()->check()) <input type="hidden" id="user-night-mode" value="{{ auth()->user()->is_night_mode }}"> @endif
@if(auth()->check()) <input type="hidden" id="user-auth" value="1"> @endif

@include('main::sections.header')

@yield('content')

@include('main::sections.footer')

@include('main::sections.scripts')

@stack('js')

</body>
</html>
