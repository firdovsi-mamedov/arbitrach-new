<header>
    <nav class="navbar navbar-expand-lg">
        <a class="navbar-brand logo" href="/">Арбитрач</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                @foreach($navigation as $item)
                    <li class="nav-item">
                        <a class="nav-link"
                           href="{{ route('index', $item->slug) }}">{{ $item->title }} <span
                                    class="sr-only"></span></a>
                    </li>

                @endforeach
                @auth
                    <li class="nav-item logout-btn">
                        <a href="#" class="nav-link" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Выйти</a>
                    </li>
                    @endauth
            </ul>
            <div class="header-btn">
                <search></search>
                <button class="notification"></button>
                @auth
                    <button onclick="location.href='{{ route('cabinet.index') }}'" class="profile"></button>
                @endauth
            </div>
        </div>
    </nav>
</header>