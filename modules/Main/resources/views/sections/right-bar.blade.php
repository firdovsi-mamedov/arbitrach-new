<div class="container-right">

    @auth
        <div class="personal-menu">
        <div class="personal-menu-header">
            <div style="cursor: pointer" onclick="location.href = '{{ route('cabinet.index') }}'">
                <img src="{{ asset('image/layer-1.svg') }}" alt="photo">
                <a class="personal-menu-name" href="{{ route('cabinet.index') }}">{{ auth()->user()->name }}</a>
            </div>
            <div class="personal-menu-btn">
                <a href="{{ route('cabinet.settings') }}" class="install"></a>
                <button class="turn-on" onclick="event.preventDefault();document.getElementById('logout-form').submit();"></button>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
        <div class="personal-menu-rating">
            <div>
                <span>{{ auth()->user()->rating }}</span> Рейтинг
            </div>
            <div>
                <span>{{ count(auth()->user()->followers) }}</span> Подписчики
            </div>
        </div>
        <div class="personal-menu-list">
            <ul>
                <li><a href="{{ route('cabinet.answers') }}">Ответы</a></li>
                <li><a href="{{ route('cabinet.comments') }}">Комментарии</a></li>
                <li><a href="{{ route('cabinet.votes') }}">Оценки</a></li>
                <li><a href="{{ route('cabinet.saves') }}">Сохраненные</a></li>
                <li><a href="{{ route('cabinet.subscriptions') }}">Подписки</a></li>
                <li><a href="{{ route('cabinet.ignored') }}">Игнор-лист</a></li>
                <li><a href="{{ route('cabinet.notes') }}">Заметки</a></li>
            </ul>
        </div>
    </div>
    @endauth

    @auth
        <div class="personal-add-post">
            <button onclick="location.href='{{ route('posts.create') }}'">Добавить пост</button>
        </div>
        <div class="personal-have-seen" style="display: block">
            <div class="inline-select">
                <div class="select-label">Просмотренные</div>
                <viewed-posts></viewed-posts>
            </div>
            @if(Auth::user()->is_settings_on_menu)
                <div class="inline-select">
                    <div class="select-label">Клубничка</div>
                    <erotic-posts></erotic-posts>
                </div>
            @endif
        </div>
    @endauth
    @guest
        <div class="auth-component">
            <auth></auth>
        </div>
    @endguest
    <div class="personal-banner">
        <img src="{{ asset('image/banner1.png') }}" alt="banner">
    </div>
    <div class="keyboard">
        <h6>Управление клавиатурой</h6>
        @if(Auth::user() && Auth::user()->is_night_mode)
            <img src="{{ asset('image/night-keyboard.png') }}" alt="keyboard">
            @else
        <img src="{{ asset('image/keyboard.png') }}" alt="keyboard">
            @endif
    </div>
    @auth
        <div class="personal-tags">
            @foreach(auth()->user()->subscribedTags as $tag)
                <a href="{{ route('tags.show', $tag->slug) }}">{{ $tag->name }}</a>
            @endforeach
        </div>
    @endauth
</div>