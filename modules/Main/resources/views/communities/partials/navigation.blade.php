<nav class="navbar navbar-expand-lg">
    <div class="collapse navbar-collapse" id="navbarNav2">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link {{ Route::currentRouteName() === 'communities.index' ? 'active' : '' }}"
                   href="{{ route('communities.index') }}"
                   title="Посты" style="text-align: center">Посты</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Route::currentRouteName() === 'communities.all' ? 'active' : '' }}"
                   href="{{ route('communities.all') }}"
                   title="Все сообщества">Все сообщества</a>
            </li>
            @auth
            <li class="nav-item">
                <a class="nav-link {{ Route::currentRouteName() === 'communities.mine' ? 'active' : '' }}"
                   href="{{ route('communities.mine') }}"
                   title="Мои сообщества">Мои сообщества</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Route::currentRouteName() === 'communities.create' ? 'active' : '' }}"
                   href="{{ route('communities.create') }}"
                   title="Создать сообщество">Создать сообщество</a>
            </li>
            @endauth
        </ul>
    </div>
</nav>