@extends('main::layouts.master')

@section('content')

    @include('main::communities.partials.navigation')

    <div class="container-fluid">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <store-community></store-community>
    </div>
@endsection