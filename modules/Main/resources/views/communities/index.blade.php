@extends('main::layouts.master')

@section('content')
    @include('main::communities.partials.navigation')

    <post-list api-url="{{ route('api.communities.posts') }}"></post-list>

@endsection