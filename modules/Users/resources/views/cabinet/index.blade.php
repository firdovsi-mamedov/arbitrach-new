@extends('main::layouts.master')

@section('content')
    @include('users::_partial.user-info')

    <nav class="navbar navbar-expand-lg">
        <a class="nav-link first-link @if(request('sort') === null || request('sort') === 'time') active @endif"
           href="{{ route('cabinet.index', ['time']) }}"
           title="По времени">По времени <span class="sr-only">(current)</span>
        </a>
        <div class="collapse navbar-collapse" id="navbarNav2">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link @if(request('sort') === 'rating') active @endif"
                       href="{{ route('cabinet.index', ['rating']) }}" title="По рейтингу">По рейтингу
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @if(request('sort') === 'my') active @endif"
                       href="{{ route('cabinet.index', ['my']) }}" title="Мое">Мое
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @if(request('sort') === 'filter') active @endif"
                       href="{{ route('cabinet.index', 'filter') }}"
                       title="Поиск">Поиск
                    </a>
                </li>
            </ul>
            {{--<div class="sort-by">--}}
                {{--<button class="sort-btn"><span>От старых к новым</span></button>--}}
            {{--</div>--}}
        </div>
    </nav>

    <post-list api-url="{{ route('api.users.posts', [request('sort')]) }}">
        @if(request('sort') === 'filter')
            <community-post-filter style="margin-bottom: 20px" slot="filter"></community-post-filter>
        @endif
    </post-list>
@stop