@extends('main::layouts.master')

@section('content')
    <nav class="navbar navbar-expand-lg">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link @if(request('filter') === null) active @endif" href="{{ route('cabinet.votes') }}" title="Понравилось">Понравилось</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @if(request('filter')) active @endif" href="{{ route('cabinet.votes', ['filter' => 'disliked']) }}" title="Не понравилось">Не понравилось</a>
                </li>
            </ul>
    </nav>
    <likes-list
            mode="{{ request('filter') }}"
            api-url="{{ route('api.likes.posts') }}"
    ></likes-list>
@stop