@extends('main::layouts.master')

@section('content')
    <nav class="navbar navbar-expand-lg">
        <a class="nav-link first-link @if(request('sort') === null || request('sort') === 'date') active @endif"
           href="{{ route('cabinet.comments', ['date']) }}" title="По времени">По времени <span
                    class="sr-only">(current)</span></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav3"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav3">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link @if(request('sort') === 'rating') active @endif" href="{{ route('cabinet.comments', ['rating']) }}"
                       title="По рейтингу">По рейтингу</a>
                </li>
            </ul>
        </div>
    </nav>

    <user-comment-list
            style="margin-top: 15px"
            mode="user"
            sort="{{ request('sort') }}"
            api-url="{{ route('api.comments.my') }}"
    ></user-comment-list>
@stop