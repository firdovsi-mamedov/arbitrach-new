@extends('main::layouts.master')

@section('content')
    @include('users::_partial.user-info')

    <user-settings></user-settings>
@stop