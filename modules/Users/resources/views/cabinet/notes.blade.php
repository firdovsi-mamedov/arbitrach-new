@extends('main::layouts.master')

@section('content')
    <note-list api-url="{{ route('api.notes.index') }}"></note-list>
@endsection