<div class="personal-room">
    <div class="personal-room-header">
        <img src="{{ asset($user->avatar) }}" alt="personal photo">
        <div class="personal-room-title">
            <span onclick="location.href = '{{ route('cabinet.index') }}'" style="cursor: pointer">
                {{ $user->name }}
            </span>
            арбитрачит
            {{ \Jenssegers\Date\Date::parse($user->created_at)->ago(null, true) }}
        </div>
        <a href="{{ route('cabinet.settings') }}" class="install"></a>
    </div>
    <div class="personal-room-photo"><img src="{{ asset('image/img-title.png') }}" alt="title"></div>
</div>
<div class="personal-statistic">
    <div class="personal-statistic-text">
        Поставил
        <span class="arrow-up">{{ $plus }}</span> плюсов
        <span class="arrow-down">{{ $minus }}</span> минусов
        @if(Auth::check() && Route::currentRouteName() === 'users.show')
        <user-actions
                id="{{ $user->id }}"
                subscribed="{{ in_array($user->id, Auth::user()->followers->pluck('id')->toArray()) }}"
                ignored="{{ in_array($user->id, Auth::user()->ignores->pluck('id')->toArray()) }}"
        ></user-actions>
        @endif
    </div>
    <div class="personal-statistic-block">
        <div><span>{{ $user->rating }}</span>рейтинг</div>
        <div><span>{{ $user->comments->count() }}</span>комментариев</div>
        <div><span>{{ $user->posts->count() }}</span>постов</div>
        <div><span>0</span>в "горячем"</div>
    </div>


    @if(Route::currentRouteName() === 'users.show')
        <note-input id="{{ $user->id }}"></note-input>
    @endif

</div>