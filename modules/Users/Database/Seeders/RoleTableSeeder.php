<?php

namespace Modules\Users\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Users\Entities\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        Role::truncate();

        $roles = ['admin', 'moderator', 'partner', 'user'];

        foreach ($roles as $role) {
            Role::create(['name' => $role]);
        }
    }
}
