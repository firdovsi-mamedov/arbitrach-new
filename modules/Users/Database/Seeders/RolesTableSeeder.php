<?php

namespace Modules\Users\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Users\Entities\Role;

class RolesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        Role::truncate();

        $roles = [
            'admin'     => 'Администратор',
            'moderator' => 'Модератор',
            'partner'   => 'Партнер',
            'user'      => 'Пользователь',
        ];

        foreach ($roles as $key => $role) {
            Role::create([
                'name'  => $key,
                'label' => $role
            ]);
        }
    }
}
