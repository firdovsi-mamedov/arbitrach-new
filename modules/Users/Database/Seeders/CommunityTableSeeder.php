<?php

namespace Modules\Users\Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Post\Entities\Post;
use Modules\Post\Entities\Tag;
use Modules\Users\Entities\Community;
use Modules\Users\Entities\User;

class CommunityTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function run()
    {
        Model::unguard();
        Community::truncate();

        $content     = \File::get(public_path('image/layer-1.svg'));
        $path        = 'public/communities/default_image.svg';
        $tags        = Tag::all();
        $users       = User::all();
        $posts       = Post::all();
        $communities = collect();

        \Storage::put($path, $content);

        $faker = Factory::create('ru');

        foreach (range(1, 10) as $i) {
            $community = Community::create([
                'name'    => $faker->text(20),
                'slug'    => $faker->unique()->slug,
                'image'   => $path,
                'user_id' => $users->random()->id
            ]);
            $communities->push($community);

            $community->users()->saveMany($users->take(rand(5, 20)));
            $community->tags()->saveMany($tags->take(rand(5, 30)));
        }

        foreach ($posts as $post) {
            $post->update([
                'community_id' => $communities->random()->id
            ]);
        }
    }
}
