<?php

namespace Modules\Users\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Generator as Faker;
use Modules\Users\Entities\Role;
use Modules\Users\Entities\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function run(Faker $faker)
    {
        Model::unguard();
        User::truncate();

        $content = \File::get(public_path('image/layer-1.svg'));
        $path    = 'public/users/default_avatar.svg';

        \Storage::put($path, $content);

        User::create([
            'name'     => 'admin',
            'email'    => 'admin@admin.ru',
            'password' => 'password',
            'avatar'   => $path,
            'role_id'  => Role::where('name', 'admin')->first()->id
        ]);

        for ($i = 0; $i < 20; $i++) {
            User::create([
                'name'     => $faker->unique()->name,
                'email'    => $faker->unique()->email,
                'password' => 'password',
                'avatar'   => $path
            ]);
        }
    }
}
