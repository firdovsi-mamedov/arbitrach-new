<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('avatar')->nullable();
            $table->rememberToken();

            // лента
            $table->float('rating')->default(0);
            $table->integer('show_posts_with_rating_higher')->default(0);
            $table->integer('posts_per_page')->default(10);
            $table->boolean('is_paginated')->default(false);
            $table->boolean('is_content_collapsed')->default(false);
            $table->boolean('is_show_all_long_post')->default(false);

            // пост
            $table->boolean('is_tags_on_top')->default(false);
            $table->boolean('is_gif_paused_on_posts')->default(false);
            $table->boolean('is_show_same_posts')->default(false);
            $table->string('viewed_posts_state')->default('show');
            $table->string('erotic_posts_state')->default('show');

            // комментарии
            $table->integer('show_comments_with_rating_higher')->default(0);
            $table->integer('collapse_comment_after_level')->default(3);
            $table->boolean('is_gif_paused_on_comments')->default(false);

            // другое
            $table->boolean('is_show_scroll_top')->default(false);
            $table->boolean('is_settings_on_menu')->default(false);
            $table->boolean('is_disable_ads')->default(false);
            $table->boolean('is_night_mode')->default(false);


            //кто такой
            $table->unsignedInteger('role_id')->default(4);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
