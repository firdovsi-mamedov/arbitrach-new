<?php

namespace Modules\Users\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CommunityResource extends Resource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'   => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'show' => route('communities.show', $this->slug)
        ];
    }
}
