<?php

namespace Modules\Users\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Users\Entities\User;
use Modules\Users\Http\Resources\UserResource;

class UsersController extends Controller
{

    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(User $user)
    {
        $plus  = $user->likes()->count();
        $minus = $user->dislikes()->count();

        return view('users::show', compact('plus', 'minus', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function update(Request $request)
    {
        \Auth::user()->update($request->all());

        return response()->json(['success' => true]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function search(Request $request)
    {
        $users = User::select('*');

        if ($search = $request->get('search', false)) {
            $users = $users->where('name', 'like', "%$search%");
        }

        $users = $users->get();

        return UserResource::collection($users);
    }
}
