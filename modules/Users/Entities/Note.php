<?php

namespace Modules\Users\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Modules\Users\Entities\Note
 *
 * @property int $id
 * @property int $user_id
 * @property int $notifier_id
 * @property string $text
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Modules\Users\Entities\User $notifier
 * @property-read \Modules\Users\Entities\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Note whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Note whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Note whereNotifierId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Note whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Note whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Note whereUserId($value)
 * @mixin \Eloquent
 */
class Note extends Model
{

    protected $fillable = ['user_id', 'notifier_id', 'text'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notifier()
    {
        return $this->belongsTo(User::class, 'notifier_id');
    }
}
