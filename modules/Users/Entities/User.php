<?php

namespace Modules\Users\Entities;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Modules\Post\Entities\Comment;
use Modules\Post\Entities\Like;
use Modules\Post\Entities\Post;
use Modules\Post\Entities\Tag;

/**
 * Modules\Users\Entities\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $avatar
 * @property string|null $remember_token
 * @property float $rating
 * @property int $show_posts_with_rating_higher
 * @property int $posts_per_page
 * @property int $is_paginated
 * @property int $is_content_collapsed
 * @property int $is_show_all_long_post
 * @property int $is_tags_on_top
 * @property int $is_gif_paused_on_posts
 * @property int $is_show_same_posts
 * @property int $show_comments_with_rating_higher
 * @property int $collapse_comment_after_level
 * @property int $is_gif_paused_on_comments
 * @property int $is_show_scroll_top
 * @property int $is_settings_on_menu
 * @property int $is_disable_ads
 * @property int $is_night_mode
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Post\Entities\Comment[] $comments
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Users\Entities\Community[] $communities
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Users\Entities\Community[] $community
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Users\Entities\User[] $followers
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Post\Entities\Like[] $likes
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Users\Entities\Note[] $notes
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Users\Entities\Note[] $notifiers
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Post\Entities\Post[] $posts
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Post\Entities\Tag[] $subscribedTags
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Users\Entities\User[] $subscribedUsers
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereCollapseCommentAfterLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereIsContentCollapsed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereIsDisableAds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereIsGifPausedOnComments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereIsGifPausedOnPosts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereIsNightMode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereIsPaginated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereIsSettingsOnMenu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereIsShowAllLongPost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereIsShowSamePosts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereIsShowScrollTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereIsTagsOnTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User wherePostsPerPage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereShowCommentsWithRatingHigher($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereShowPostsWithRatingHigher($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable
{

    use Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'avatar',
        'rating',
        'role_id',
        'show_posts_with_rating_higher',
        'posts_per_page',
        'is_paginated',
        'is_content_collapsed',
        'is_show_all_long_post',
        'is_tags_on_top',
        'is_gif_paused_on_posts',
        'is_show_same_posts',
        'show_comments_with_rating_higher',
        'collapse_comment_after_level',
        'is_gif_paused_on_comments',
        'is_show_scroll_top',
        'is_settings_on_menu',
        'is_disable_ads',
        'is_night_mode',
        'viewed_posts_state',
        'erotic_posts_state'
    ];

    protected $appends = ['show'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function getShowAttribute()
    {
        return route('users.show', $this->id);
    }

    /**
     * @return array
     */
    public function getBoolFields()
    {
        return array_values(array_filter($this->fillable, function ($item) {
            return strpos($item, 'is_') !== false;
        }));
    }

    /**
     * This mutator automatically hashes the password.
     *
     * @var string
     */
    public function setPasswordAttribute($value)
    {
        if (mb_strlen($value) > 0) {
            $this->attributes['password'] = \Hash::make($value);
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function community()
    {
        return $this->hasMany(Community::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function communities()
    {
        return $this->belongsToMany(Community::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notes()
    {
        return $this->hasMany(Note::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notifiers()
    {
        return $this->hasMany(Note::class, 'notifier_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function subscribedTags()
    {
        return $this->belongsToMany(Tag::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function likes()
    {
        return $this->hasMany(Like::class)->where('vote', 1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dislikes()
    {
        return $this->hasMany(Like::class)->where('vote', -1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function likedPosts()
    {
        return $this->likes()->where('likeable_type', Post::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dislikedPosts()
    {
        return $this->dislikes()->where('likeable_type', Post::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function savedComments()
    {
        return $this->morphedByMany(Comment::class, 'saveable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function savedPosts()
    {
        return $this->morphedByMany(Post::class, 'saveable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function subscribedUsers()
    {
        return $this->belongsToMany(
            User::class,
            'subscriber_user',
            'user_id',
            'subscriber_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function followers()
    {
        return $this->subscribedUsers()->where('ignored', false);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function ignores()
    {
        return $this->subscribedUsers()->where('ignored', true);
    }

    /**
     * @param $value
     * @return string
     */
    public function getAvatarAttribute($value)
    {
        return \Storage::url($value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function viewedPosts()
    {
        return $this->belongsToMany(Post::class);
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->role->name === 'admin';
    }
}
