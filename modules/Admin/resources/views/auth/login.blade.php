<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{ asset('/assets/img/favicon.png') }}">
    <title>Admin Panel</title>

    <!-- Icons -->
    <link href="{{ asset('assets/vendors/css/flag-icon.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/css/simple-line-icons.min.css') }}" rel="stylesheet">


    <!-- Main styles for this application -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/admin.css') }}" rel="stylesheet">
</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
<header class="app-header navbar">
    <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">
        <span class="navbar-toggler-icon"></span>
    </button>
        <div class="col-sm-4">
            <a href="/" style="text-decoration: none">На сайт</a>
        </div>
</header>
<div class="app-body">

    <!-- Main content -->
    <main class="main">

        <br>

        <div class="container-fluid">
{{-------------------------------------------------------------}}
            <div class="row">
                <div class="col-md-8 offset-2">
                    <br>
                    <div class="row form-group" style="background-color: white; border-radius: 10px;">
                        <h3 class="text-center">Вход</h3>
                        <br><br>
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('login',['status'=>'admin']) }}" style="width: 900px !important;">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Адрес</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Пароль</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Запомнить меня
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Вход
                                    </button>

                                    <a class="btn btn-link" href="{{ route('password.request',['status'=>'admin']) }}">
                                        Забыли пароль?
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            {{-------------------------------------------------------------}}
        </div>
    </main>

</div>
</body>
</html>


