<div class="col-sm-12 profile">
    <div class="row card">
        <div class="card-header" style="display: flex; flex-direction: row;justify-content: space-between;">
            <h4 style="margin: 0;    padding-top: 2px;">Посты</h4>
        </div>
        <div class="table-responsive">
            <table class="table" style="margin-bottom:0;">
                <tr>
                    <th>Название</th>
                    <th>Автор</th>
                    <th>Путь</th>
                    <th>Рейтинг</th>
                    <th>Черновик</th>
                    <th>Мой</th>
                    <th>Время публикации</th>
                    <th>Действия</th>
                </tr>
                @foreach ($posts as $key)
                    <tr>
                        <td>
                            <strong class="badge">
                                <a href="{{ route('admin.posts.edit', ['slug' => $key->slug]) }}" target="_blank">
                                    {{ str_limit($key->title,10,'...') }}
                                </a>
                            </strong>
                        </td>
                        <td>@if($key->user) {{ str_limit($key->user->name,50,'...') }} @else - @endif</td>
                        <td>{{ str_limit($key->slug,15,'...') }}</td>
                        <td>{{ $key->rating }}</td>
                        <td>{{ $key->draft ? 'Да' : 'Нет' }}</td>
                        <td>{{ $key->my ? 'Да' : 'Нет' }}</td>
                        <td>{{ $key->created_at }}</td>
                        <td>
                            <div class="btn-group" role="group">
                                <a href="{{ route('admin.posts.edit', ['slug' => $key->slug]) }}"
                                   class="btn btn-sm btn-success">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <form action="{{ route('admin.posts.destroy', $key->id) }}" method="post" style="margin-left:5px !important;">
                                    @csrf
                                    @method('delete')
                                    <input type="hidden" name="id" value="{{ $key->id }}">
                                    <button type="submit" class="btn btn-sm btn-danger"><i
                                                class="fa fa-trash"></i></button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>