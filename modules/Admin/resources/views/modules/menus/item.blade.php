@extends('admin::layouts.master')

@section('content')
    <div class="col-sm-12 profile">
        <div class="row card">
            <div class="card-header">
                <strong>Меню</strong>
            </div>
            <div class="card-body">
                @include('admin::modules.menus.components._form')
            </div>
        </div>
    </div>
    <style>
        .profile .card-body .col-3 img {
            height: 300px;
            max-width: 100%;
        }
    </style>
@endsection