<form action="{{ $menu->id ? route('admin.menus.update', $menu->slug) : route('admin.menus.store')}}"
      method="POST">
    @csrf
    @if($menu->id)
        @method('put')
    @endif
    <div class="row">
        <div class="col-md-8 offset-2">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul style="list-style-type: none">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if($menu->id)
                <div class="row form-group">
                    <strong>Id</strong>
                    <input class="form-control" type="text" value="{{ $menu->id }}" disabled>
                </div>
            @endif
            <div class="row form-group">
                <strong>Имя</strong>
                <input name="title" class="form-control" type="text" value="{{ $menu->title }}">
            </div>
            <div class="row form-group">
                <strong>Порядок</strong>
                <input name="order" class="form-control" type="text" value="{{ $menu->order }}">
            </div>
            <div class="row form-group">
                <strong>Путь</strong>
                <input name="slug" class="form-control" type="text" value="{{ $menu->slug }}">
            </div>
            <div class="row form-check">
                <label>
                    <input name="active" class="form-check-input" type="checkbox" @if($menu->active) checked @endif>
                    <strong>Активный</strong>
                </label>
            </div>
            @if($menu->id)
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample"
                        aria-expanded="false" aria-controls="collapseExample" style="width: 100% !important;">
                    Категории
                </button>
                <br>
                <hr>
                <div class="collapse" id="collapseExample">
                    <div class="card card-body">
                        <ul class="list-group w-200">
                            @foreach($categories as $key)
                                <li class="list-group-item">
                                    <div class="list-group-item list-group-item-action">
                                        <div class="form-check text-center">
                                            <div class="form-check">
                                                <input class="form-check-input"
                                                       type="checkbox"
                                                       value="{{ $key->id }}"
                                                       id="defaultCheck1"
                                                       {{ in_array($key->id, $menu->categories->pluck('id')->toArray()) ? 'checked' : '' }}
                                                       name="checked[]">
                                                <label class="form-check-label" for="defaultCheck1">
                                                    {{ $key->name }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
            <div class="row form-group">
                @if($menu->id)
                    <input type="hidden" name="id" value="{{ $menu->id }}">
                @else
                @endif
                <button class="btn btn-success btn-block" type="submit">Сохранить</button>
            </div>
        </div>
    </div>
</form>


