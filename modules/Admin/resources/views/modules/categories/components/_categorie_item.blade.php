<div class="col-sm-12 profile">
    <div class="row card">
        <div class="card-header" style="display: flex; flex-direction: row;justify-content: space-between;">
            <h4 style="margin: 0;    padding-top: 2px;">Категории</h4>
            <a href="{{ route('admin.categories.create') }}" class="btn btn-primary" style="margin-top: 0;">
                <span class="fa fa-plus"></span>
                Добавить
            </a>
        </div>
        <div class="table-responsive">
            <table class="table" style="margin-bottom:0;">
                <tr>
                    <th>Название</th>
                    <th>Порядок</th>
                    <th>Активен</th>
                    <th>Путь</th>
                    <th>Действия</th>
                </tr>
                @foreach ($categories as $key)
                    <tr>
                        <td>
                            <strong class="badge">
                                <a href="{{ route('admin.categories.edit', ['slug' => $key->slug]) }}" target="_blank">
                                    {{ $key->name }}
                                </a>
                            </strong>
                        </td>
                        <td>{{ $key->order }}</td>
                        <td>{{ $key->active ? 'Да' : 'Нет' }}</td>
                        <td>{{ $key->slug }}</td>
                        <td>
                            <div class="btn-group" role="group">
                                <a href="{{ route('admin.categories.edit', ['slug' => $key->slug]) }}"
                                   class="btn btn-sm btn-success">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <form action="{{ route('admin.categories.destroy', $key->id) }}" method="post" style="margin-left:5px !important;">
                                    @csrf
                                    @method('delete')
                                    <input type="hidden" name="id" value="{{ $key->id }}">
                                    <button type="submit" class="btn btn-sm btn-danger"><i
                                                class="fa fa-trash"></i></button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>