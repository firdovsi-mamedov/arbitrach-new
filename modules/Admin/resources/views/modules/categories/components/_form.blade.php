<form action="{{ $category->id ? route('admin.categories.update', $category->slug) : route('admin.categories.store')}}"
      method="POST">
    @csrf
    @if($category->id)
        @method('put')
    @endif
    <div class="row">
        <div class="col-md-8 offset-2">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul style="list-style-type: none">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if($category->id)
                <div class="row form-group">
                    <strong>Id</strong>
                    <input class="form-control" type="text" value="{{ $category->id }}" disabled>
                </div>
            @endif
            <div class="row form-group">
                <strong>Имя</strong>
                <input name="name" class="form-control" type="text" value="{{ $category->name }}">
            </div>
            <div class="row form-group">
                <strong>Порядок</strong>
                <input name="order" class="form-control" type="text" value="{{ $category->order }}">
            </div>
            <div class="row form-group">
                <strong>Путь</strong>
                <input name="slug" class="form-control" type="text" value="{{ $category->slug }}">
            </div>
            <div class="row form-check">
                <label>
                    <input name="active" class="form-check-input" type="checkbox" @if($category->active) checked @endif>
                    <strong>Активный</strong>
                </label>
            </div>
            @if($category->id)
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample"
                        aria-expanded="false" aria-controls="collapseExample" style="width: 100% !important;">
                    Пункты Меню
                </button>
                <br>
                <hr>
                <div class="collapse" id="collapseExample">
                    <div class="card card-body">
                        <ul class="list-group w-200">
                            @foreach($menus as $key)
                                <li class="list-group-item">
                                    <div class="list-group-item list-group-item-action">
                                        <div class="form-check text-center">
                                            <div class="form-check">
                                                <input class="form-check-input"
                                                       type="checkbox"
                                                       value="{{ $key->id }}"
                                                       id="defaultCheck1"
                                                       {{ in_array($key->id, $category->menus->pluck('id')->toArray()) ? 'checked' : '' }}
                                                       name="checked[]">
                                                <label class="form-check-label" for="defaultCheck1">
                                                    {{ $key->title }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample3"
                            aria-expanded="false" aria-controls="collapseExample3" style="width: 100% !important;">
                        Посты
                    </button>
                    <br>
                    <hr>
                    <div class="collapse" id="collapseExample3">
                        <div class="card card-body">
                            @if(count($category->posts)==0)
                                <h4 class="text-center">Ничего не найдено</h4>
                            @endif
                            <div class="table-responsive">
                                <table class="tabulka_kariet table table-bordered table-hover">
                                    <thead>
                                    <tr class="alert alert-secondary" id="loadAjaxCategoryPosts" style="display:none">
                                        <td colspan="2" style="width:100%; text-align:center"><b><i
                                                        class="fa fa-spinner fa-spin" style="font-size:20px"></i></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Наименование</th>
                                        <th>Отношение</th>
                                    </tr>
                                    </thead>
                                    @foreach($posts as $key)
                                        <tr>
                                            <td>{{ $key->title }}</td>
                                            <td style='text-align:center'>
                                                <div class="form-check">
                                                    <input class="form-check-input"
                                                           type="checkbox"
                                                           id="defaultCheckkkkkk{{ $key->id }}"
                                                           @if(count($category->posts)>0)
                                                           @if($key->category_id == $category->id)
                                                           checked
                                                           @endif
                                                           @endif
                                                           onchange="CheckSwitch({{ $category->id }},{{ $key->id }},'loadAjaxCategoryPosts',{{ json_encode($CategoryPostsUrl) }},this)">
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                @endif
            <div class="row form-group">
                @if($category->id)
                    <input type="hidden" name="id" value="{{ $category->id }}">
                @else
                @endif
                <button class="btn btn-success btn-block" type="submit">Сохранить</button>
            </div>
        </div>
    </div>
</form>

