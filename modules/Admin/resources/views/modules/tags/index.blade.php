@extends('admin::layouts.master')

@section('content')
    <div class="col-12">
        <div class="card">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul style="list-style-type: none">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card-header">
                @include('admin::modules.tags.components._search')
            </div>
            <div class="card-body">
                @if (!count($tags))
                    <h4 class="text-center">Ничего не найдено</h4>
                @else
                    @include('admin::modules.tags.components._tag_item')
                @endif
            </div>
        </div>
        {{ $tags->appends(Request::except('page'))->links() }}
    </div>
@endsection