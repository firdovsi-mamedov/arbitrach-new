<div class="col-sm-12 profile">
    <div class="row card">
        <div class="card-header" style="display: flex; flex-direction: row;justify-content: space-between;">
            <h4 style="margin: 0;    padding-top: 2px;">Теги</h4>
            <a href="{{ route('admin.tags.create') }}" class="btn btn-primary" style="margin-top: 0;">
                <span class="fa fa-plus"></span>
                Добавить
            </a>
        </div>
        <div class="table-responsive">
            <table class="table" style="margin-bottom:0;">
                <tr>
                    <th>Название</th>
                    <th>Путь</th>
                    <th>Порядок</th>
                    <th>Активен</th>
                    <th>Действия</th>
                </tr>
                @foreach ($tags as $tag)
                    <tr>
                        <td>
                            <strong class="badge">
                                <a href="{{ route('admin.tags.edit', $tag->id) }}" target="_blank">
                                    {{ $tag->name }}
                                </a>
                            </strong>
                        </td>
                        <td>{{ $tag->slug }}</td>
                        <td>{{ $tag->order }}</td>
                        <td>{{ $tag->active ? 'Да' : 'Нет' }}</td>
                        <td>
                            <a href="{{ route('admin.tags.edit', $tag->slug) }}" class="btn btn-sm btn-success">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a href="#" onclick="document.getElementById('del-form-{{$tag->id}}').submit()" class="btn btn-sm btn-danger">
                                <i class="fa fa-trash"></i>
                            </a>
                            <form action="{{ route('admin.tags.destroy', $tag) }}" method="post" style="display: none;" id="del-form-{{$tag->id}}">
                                @csrf
                                @method('delete')
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>