<div class="col-sm-12 profile">
    <div class="row card">
        <div class="card-header" style="display: flex; flex-direction: row;justify-content: space-between;">
            <h4 style="margin: 0;    padding-top: 2px;">Сообщества</h4>
        </div>
        <div class="table-responsive">
            <table class="table" style="margin-bottom:0;">
                <tr>
                    <th>Название</th>
                    <th>Создатель</th>
                    <th>Приватное</th>
                    <th>Путь</th>
                    <th>Создано</th>
                    <th>Действия</th>
                </tr>
                @foreach ($communities as $key)
                    <tr>
                        <td>
                            <strong class="badge">
                                <a href="{{ route('admin.communities.edit', ['slug' => $key->slug]) }}" target="_blank">
                                    <img src="{{ asset($key->image) }}" class="img-rounded" style="margin-right:10px; max-width:16px; max-height:16px;">
                                    {{ str_limit($key->name,20,'...') }}
                                </a>
                            </strong>
                        </td>
                        <td>
                         @if($key->user) {{ str_limit($key->user->name,50,'...') }} @else - @endif
                        </td>
                        <td>{{ $key->private ? 'Да':'Нет' }}</td>
                        <td>{{ str_limit($key->slug,15,'...') }}</td>
                        <td>{{ $key->created_at }}</td>
                        <td>
                            <div class="btn-group" role="group">
                                <a href="{{ route('admin.communities.edit', ['slug' => $key->slug]) }}"
                                   class="btn btn-sm btn-success">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <form action="{{ route('admin.communities.destroy', $key->id) }}" method="post" style="margin-left:5px !important;">
                                    @csrf
                                    @method('delete')
                                    <input type="hidden" name="id" value="{{ $key->id }}">
                                    <button type="submit" class="btn btn-sm btn-danger"><i
                                                class="fa fa-trash"></i></button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
