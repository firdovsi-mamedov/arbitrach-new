@if($comment->image)
<table>
    @else
        <table style="width:100% !important;">
        @endif
    <tr>
        @if($comment->image)
        <td>
            @include('admin::modules.comments.components.image')
        </td>
        @endif
        <td style="width: 150%;">
            <form action="{{ route('admin.comments.update', $comment->id) }}" method="POST">
                @csrf
                @method('put')
                <div class="row">
                    <div class="col-md-8 offset-2">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul style="list-style-type: none">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="row form-group">
                            <strong>Id</strong>
                            <input class="form-control" type="text" value="{{ $comment->id }}" disabled>
                        </div>
                        <div class="row form-group">
                            <strong>Рейтинг</strong>
                            <input name="rating" class="form-control" type="text" value="{{ $comment->rating }}">
                        </div>
                        <div class="row form-group">
                            <strong>Текст</strong>
                            <textarea name="text" class="form-control">{{ $comment->text }}</textarea>
                        </div>
                        <button class="btn btn-primary" type="button" data-toggle="collapse"
                                data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"
                                style="width: 100% !important;">
                            Ответы
                        </button>
                        <br>
                        <hr>
                        <div class="collapse" id="collapseExample">
                            <div class="card card-body">
                                <div class="table-responsive">
                                    <table class="tabulka_kariet table table-bordered table-hover">
                                        <thead>
                                        <tr class="alert alert-secondary" style="display:none">
                                            <td colspan="2" style="width:100%; text-align:center"><b><i
                                                            class="fa fa-spinner fa-spin"
                                                            style="font-size:20px"></i></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>№</th>
                                            <th>Рейтинг</th>
                                            <th>Текст</th>
                                            <th>Перейти</th>
                                        </tr>
                                        </thead>
                                        @foreach($comments as $key)
                                         @if($key->parent_id != null)
                                         @if(($comment->id == $key->parent_id) && ($comment->id != $key->id))
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $key->rating }}</td>
                                                <td>{{ str_limit($key->text,120,'...') }}</td>
                                                <td>
                                                    <a class="btn btn-sm btn-info"
                                                       href="{{ route('admin.comments.edit',$key) }}" target="_blank">
                                                        <i class="fa fa-edit" style="color:white"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                         @endif
                                         @endif
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="{{ $comment->id }}">
                        <button class="btn btn-success btn-block" type="submit">Сохранить</button>
                    </div>
                </div>
            </form>
        </td>
    </tr>
</table>
