<div class="col-sm-12 profile">
    <div class="row card">
        <div class="card-header" style="display: flex; flex-direction: row;justify-content: space-between;">
            <h4 style="margin: 0;    padding-top: 2px;">Пользователи</h4>
            {{-- <a href="{{ route('admin.users.create') }}" class="btn btn-primary" style="margin-top: 0;">
                 <span class="fa fa-plus"></span>
                 Добавить
             </a>--}}
        </div>
        <div class="table-responsive">
            <table class="table" style="margin-bottom:0;">
                <tr>
                    <th>Имя</th>
                    <th>Email</th>
                    <th>Права</th>
                    <th>Рейтинг</th>
                    <th>Зарегистрирован</th>
                    <th>Действия</th>
                </tr>
                @foreach ($users as $user)
                    @if($user != Auth::user())
                        <tr>
                            <td>
                                <strong class="badge">
                                    <a href="{{ route('admin.users.edit', $user->id) }}" target="_blank">
                                        <img src="{{ $user->avatar }}" class="img-rounded"
                                             style="margin-right:10px; max-width:16px; max-height:16px;">
                                        {{ $user->name }}
                                    </a>
                                </strong>
                            </td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->role->label }}</td>
                            <td>{{ $user->rating }}</td>
                            <td>{{ $user->created_at }}</td>
                            <td>
                                <a href="{{ route('admin.users.edit', $user->id) }}" class="btn btn-sm btn-success">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="#" onclick="document.getElementById('del-form-{{$user->id}}').submit()"
                                   class="btn btn-sm btn-danger">
                                    <i class="fa fa-trash"></i>
                                </a>
                                <form action="{{ route('admin.users.destroy', $user->id) }}" method="post"
                                      style="display: none;" id="del-form-{{$user->id}}">
                                    @csrf
                                    @method('delete')
                                </form>
                            </td>
                        </tr>
                    @endif
                @endforeach
            </table>
        </div>
    </div>
</div>