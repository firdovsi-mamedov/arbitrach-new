<form action="{{ $user->id ? route('admin.users.update', $user->id) : route('admin.users.store')}}" method="POST">
    @csrf
    @if($user->id)
        @method('put')
    @endif
    <div class="row">
        <div class="col-md-8 offset-2">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if($user->id)
                <div class="row form-group">
                    <strong>Id</strong>
                    <input class="form-control" type="text" value="{{ $user->id }}" disabled>
                </div>
            @endif
            <div class="row form-group">
                <strong>Имя</strong>
                <input name="name" class="form-control" type="text" value="{{ $user->name }}">
            </div>
            <div class="row form-group">
                <strong>Email</strong>
                <input name="email" class="form-control" type="text" value="{{ $user->email }}">
            </div>
            <div class="row form-group">
                <strong>Рейтинг</strong>
                <input name="rating" class="form-control" type="text" value="{{ $user->rating }}">
            </div>
            <div class="form-group row">
                <strong>Роль</strong>
                <select class="form-control" name="role_id">
                    @foreach($roles as $role)
                        <option @if($user->role->name === $role->name) selected @endif value="{{ $role->id }}">{{ $role->label }}</option>
                    @endforeach
                </select>
            </div>
            <div class="row form-group">
                <strong>Показывать посты с рейтингом выше:</strong>
                <input name="show_posts_with_rating_higher" class="form-control" type="text"
                       value="{{ $user->show_posts_with_rating_higher }}">
            </div>
            <div class="row form-group">
                <strong>Постов на страницу:</strong>
                <input name="posts_per_page" class="form-control" type="text" value="{{ $user->posts_per_page }}">
            </div>
            <div class="row form-group">
                <strong>Показывать комментарии с рейтингом выше:</strong>
                <input name="show_comments_with_rating_higher" class="form-control" type="text"
                       value="{{ $user->show_comments_with_rating_higher }}">
            </div>
            <div class="row form-group">
                <strong>Сворачивать вложенные комментарии после (уровень):</strong>
                <input name="collapse_comment_after_level" class="form-control" type="text"
                       value="{{ $user->collapse_comment_after_level }}">
            </div>
            <div class="row form-group">
                <strong>Пароль</strong>
                <input name="password" class="form-control" type="password">
            </div>
            <div class="form-group">
                @include('admin::modules.users.components._checkbox', ['title' => 'Постраничный просмотр', 'field' => 'is_paginated'])
                @include('admin::modules.users.components._checkbox', ['title' => 'Сворачивать содержимое постов', 'field' => 'is_content_collapsed'])
                @include('admin::modules.users.components._checkbox', ['title' => 'Показывать полностью длинные посты', 'field' => 'is_show_all_long_post'])
                @include('admin::modules.users.components._checkbox', ['title' => 'Показывать теги вверху поста', 'field' => 'is_tags_on_top'])
                @include('admin::modules.users.components._checkbox', ['title' => 'Ставить GIF на паузу в постах', 'field' => 'is_gif_paused_on_posts'])
                @include('admin::modules.users.components._checkbox', ['title' => 'Показывать похожие посты под комментариями', 'field' => 'is_show_same_posts'])
                @include('admin::modules.users.components._checkbox', ['title' => 'Ставить GIF на паузу в комментариях', 'field' => 'is_gif_paused_on_comments'])
                @include('admin::modules.users.components._checkbox', ['title' => 'Показывать полосу прокрутки наверх', 'field' => 'is_show_scroll_top'])
                @include('admin::modules.users.components._checkbox', ['title' => 'Вынести настройку в меню', 'field' => 'is_settings_on_menu'])
                @include('admin::modules.users.components._checkbox', ['title' => 'Отключить рекламу', 'field' => 'is_disable_ads'])
                @include('admin::modules.users.components._checkbox', ['title' => 'Ночной режим', 'field' => 'is_night_mode'])
            </div>
            @if($user->id)
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample"
                        aria-expanded="false" aria-controls="collapseExample" style="width: 100% !important;">
                    Теги
                </button>
                <br>
                <hr>
                <div class="collapse" id="collapseExample">
                    <div class="card card-body">
                        @if(count($tags)==0)
                            <h4 class="text-center">Ничего не найдено</h4>
                        @endif
                        <div class="table-responsive">
                            <table class="tabulka_kariet table table-bordered table-hover">
                                <thead>
                                <tr class="alert alert-secondary" id="loadAjaxUserTags" style="display:none">
                                    <td colspan="2" style="width:100%; text-align:center"><b><i
                                                    class="fa fa-spinner fa-spin" style="font-size:20px"></i></b>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Наименование</th>
                                    <th>Отношение</th>
                                </tr>
                                </thead>
                                @foreach ($tags as $key)
                                    <tr>
                                        <td>{{ $key->name }}</td>
                                        <td style='text-align:center'>
                                            <div class="form-check">
                                                <input class="form-check-input"
                                                       type="checkbox"
                                                       id="defaultCheckk{{ $key->id }}"
                                                       @if(count($user->subscribedTags)>0)
                                                       {{ in_array($key->id, $user->subscribedTags->pluck('id')->toArray()) ? 'checked' : '' }}
                                                       @endif
                                                       onchange="CheckSwitch({{ $user->id }},{{ $key->id }},'loadAjaxUserTags',{{ json_encode($tagsUrl) }},this)">
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample2"
                        aria-expanded="false" aria-controls="collapseExample2" style="width: 100% !important;">
                    Подписан на сообщества
                </button>
                <br>
                <hr>
                <div class="collapse" id="collapseExample2">
                    <div class="card card-body">
                        @if(count($user->communities)==0)
                            <h4 class="text-center">Ничего не найдено</h4>
                        @endif
                        <div class="table-responsive">
                            <table class="tabulka_kariet table table-bordered table-hover">
                                <thead>
                                <tr class="alert alert-secondary" id="loadAjaxUserCommunities" style="display:none">
                                    <td colspan="2" style="width:100%; text-align:center"><b><i
                                                    class="fa fa-spinner fa-spin" style="font-size:20px"></i></b>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Наименование</th>
                                    <th>Отношение</th>
                                </tr>
                                </thead>
                                @foreach($communities as $key)
                                    <tr>
                                        <td>{{ $key->name }}</td>
                                        <td style='text-align:center'>
                                            <div class="form-check">
                                                <input class="form-check-input"
                                                       type="checkbox"
                                                       id="defaultCheckkk{{ $key->id }}"
                                                       @if(count($user->communities)>0)
                                                       {{ in_array($key->id, $user->communities->pluck('id')->toArray()) ? 'checked' : '' }}
                                                       @endif
                                                       onchange="CheckSwitch({{ $user->id }},{{ $key->id }},'loadAjaxUserCommunities',{{ json_encode($communitiesUrl) }},this)">
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample1"
                        aria-expanded="false" aria-controls="collapseExample1" style="width: 100% !important;">
                    Посты
                </button>
                <br>
                <hr>
                <div class="collapse" id="collapseExample1">
                    <div class="card card-body">
                        @if(count($user->posts)==0)
                            <h4 class="text-center">Ничего не найдено</h4>
                        @endif
                        <div class="table-responsive">
                            <table class="tabulka_kariet table table-bordered table-hover">
                                <thead>
                                <tr class="alert alert-secondary" id="loadAjaxUserPosts" style="display:none">
                                    <td colspan="2" style="width:100%; text-align:center"><b><i
                                                    class="fa fa-spinner fa-spin" style="font-size:20px"></i></b>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Наименование</th>
                                    <th>Отношение</th>
                                </tr>
                                </thead>
                                @foreach($posts as $key)
                                    <tr>
                                        <td>{{ $key->title }}</td>
                                        <td style='text-align:center'>
                                            <div class="form-check">
                                                <input class="form-check-input"
                                                       type="checkbox"
                                                       id="defaultCheckkkk{{ $key->id }}"
                                                       @if(count($user->posts)>0)
                                                       @if($key->user_id == $user->id)
                                                       checked
                                                       @endif
                                                       @endif
                                                       onchange="CheckSwitch({{ $user->id }},{{ $key->id }},'loadAjaxUserPosts',{{ json_encode($postsUrl) }},this)">
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample3"
                        aria-expanded="false" aria-controls="collapseExample3" style="width: 100% !important;">
                    Созданные сообщества
                </button>
                <br>
                <hr>
                <div class="collapse" id="collapseExample3">
                    <div class="card card-body">
                        @if(count($user->community)==0)
                            <h4 class="text-center">Ничего не найдено</h4>
                        @endif
                        <div class="table-responsive">
                            <table class="tabulka_kariet table table-bordered table-hover">
                                <thead>
                                <tr class="alert alert-secondary" id="loadAjaxUserCommunity" style="display:none">
                                    <td colspan="2" style="width:100%; text-align:center"><b><i
                                                    class="fa fa-spinner fa-spin" style="font-size:20px"></i></b>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Наименование</th>
                                    <th>Отношение</th>
                                </tr>
                                </thead>
                                @foreach($community as $key)
                                    <tr>
                                        <td>{{ $key->name }}</td>
                                        <td style='text-align:center'>
                                            <div class="form-check">
                                                <input class="form-check-input"
                                                       type="checkbox"
                                                       id="defaultCheckkkkk{{ $key->id }}"
                                                       @if(count($user->community)>0)
                                                       @if($key->user_id == $user->id)
                                                       checked
                                                       @endif
                                                       @endif
                                                       onchange="CheckSwitch({{ $user->id }},{{ $key->id }},'loadAjaxUserCommunity',{{ json_encode($communityUrl) }},this)">
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            @endif
            <div class="row form-group">
                <button class="btn btn-success btn-block" type="submit">
                    @if($user->id) Изменить @else Сохранить @endif пользователя
                </button>
            </div>
        </div>
    </div>
</form>
