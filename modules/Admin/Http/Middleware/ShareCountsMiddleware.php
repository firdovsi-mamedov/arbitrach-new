<?php

namespace Modules\Admin\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Modules\Post\Entities\Category;
use Modules\Post\Entities\Tag;
use Modules\Users\Entities\User;
use Modules\Main\Entities\Menu;
use Modules\Post\Entities\Comment;
use Modules\Post\Entities\Post;
use Modules\Users\Entities\Community;

class ShareCountsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        \View::share('users_count', User::count());
        \View::share('tags_count', Tag::count());
        \View::share('category_count', Category::count());
        \View::share('menu_count', Menu::count());
        \View::share('comment_count', Comment::count());
        \View::share('post_count', Post::count());
        \View::share('community_count', Community::count());

        return $next($request);
    }
}
