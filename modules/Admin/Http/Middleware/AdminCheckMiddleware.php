<?php

namespace Modules\Admin\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AdminCheckMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (($request->user()) and ($request->user()->isAdmin())) {
            return $next($request);
        } elseif (($request->user()) and ($request->user()->isAdmin())) {
            return redirect('/');
        } else {
            return redirect()->route('login', ['status' => 'admin']);
        }
    }
}
