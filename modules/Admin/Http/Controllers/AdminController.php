<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Response;

class AdminController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::index');
    }
}
