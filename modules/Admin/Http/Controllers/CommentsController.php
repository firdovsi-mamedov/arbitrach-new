<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
//use Illuminate\Routing\Controller;
use Modules\Post\Entities\Comment;
use Modules\Admin\Http\Requests\CommentRequest;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class CommentsController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $comments = Comment::with('child')->paginate(4);

        return view('admin::modules.comments.index', compact('comments'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Comment $comment)
    {
        $comments = Comment::all();
        return view('admin::modules.comments.item', compact('comment','comments'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(CommentRequest $request, Comment $comment)
    {
        $comment->update($request->all());

        return redirect(route('admin.comments.index'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request)
    {
        $id = intval($request->id);

        if ($id == 0) {
            return redirect()->back()->withErrors('Ой');
        }

        $comment = Comment::find($id);
        $comment->delete();

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $search_string = '%'.$request->get('search').'%';

        $comments = Comment::commentsSearch($search_string)
            ->orderByDesc('id')
            ->paginate(10);

        return view('admin::modules.comments.index', compact('comments'));
    }

    public function changeImage(Request $request)
    {
        if ($file = $request->file('file', false)) {
            if ($file->isValid()) {
                $id        = intval($request->comment_id);
                $comment = Comment::find($id);

                if ($comment->image !== 'public/comments/default_image.svg') {
                    Storage::delete($comment->image);
                }

                $name = $id.'/'.str_random(10).'.'.$file->getClientOriginalExtension();
                $path = $file->storeAs('/public/comments', $name);
                $img  = Image::make(Storage::path($path));

                $img->resize(250, 120)->save();
                $comment->update(['image' => $path]);
            } else {
                return redirect()->back()->withErrors('Некорректный файл');
            }
        } else {
            return redirect()->back()->withErrors('Нет файла');
        }

        return redirect()->back();
    }
}
