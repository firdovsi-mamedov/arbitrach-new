<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Modules\Admin\Http\Requests\CommunityRequest;
use Modules\Post\Entities\Post;
use Modules\Post\Entities\Tag;
use Modules\Users\Entities\Community;
use Modules\Users\Entities\User;

//use Illuminate\Routing\Controller;


class CommunitiesController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $communities = Community::paginate(4);

        return view('admin::modules.communities.index', compact('communities'));
    }


    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Community $community)
    {
        $tagsCollection = Tag::all();
        $tags           = parent::dataSome($tagsCollection, "tags", $community, 'belongsToMany');

        $usersCollection = User::all();//belongsToMany
        $users           = parent::dataSome($usersCollection, "users", $community, 'belongsToMany');

        $postsCollection = Post::all();
        $posts           = parent::dataSome($postsCollection, "posts", $community, 'hasMany', 'community_id');

        $CommunityChangeTagsURL  = '/admin/CommunityChangeTagsCheckbox/change';
        $CommunityChangeUsersURL = '/admin/CommunityChangeUsersCheckbox/change';
        $CommunityChangePostsURL = '/admin/CommunityChangePostsCheckbox/change';

        return view('admin::modules.communities.item', compact('community', 'tags', 'users', 'posts'), [
            'CommunityChangeTagsURL'  => $CommunityChangeTagsURL,
            'CommunityChangeUsersURL' => $CommunityChangeUsersURL,
            'CommunityChangePostsURL' => $CommunityChangePostsURL
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(CommunityRequest $request, Community $community)
    {

        if ($request->filled('slug')) {
            $slug = $request->input('slug');
        } else {
            $slug = str_slug($request->input('name'));
        }

        $community->update([
            'name' => $request->name,
            'slug' => $slug
        ]);

        return redirect(route('admin.communities.index'));
    }

    public function destroy(Request $request)
    {
        $id = intval($request->id);

        if ($id == 0) {
            return redirect()->back()->withErrors('Ой');
        }

        $community = Community::find($id);
        $community->delete();

        return redirect()->back();
    }

    public function changeImage(Request $request)
    {
        if ($file = $request->file('file', false)) {
            if ($file->isValid()) {
                $id        = intval($request->community_id);
                $community = Community::find($id);

                if ($community->image !== 'public/communities/default_image.svg') {
                    Storage::delete($community->image);
                }

                $name = $id.'/'.str_random(10).'.'.$file->getClientOriginalExtension();
                $path = $file->storeAs('/public/communities', $name);
                $img  = Image::make(Storage::path($path));

                $img->resize(250, 120)->save();
                $community->update(['image' => $path]);
            } else {
                return redirect()->back()->withErrors('Некорректный файл');
            }
        } else {
            return redirect()->back()->withErrors('Нет файла');
        }

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $search_string = '%'.$request->get('search').'%';

        $communities = Community::communitySearch($search_string)
            ->orderByDesc('id')
            ->paginate(10);

        return view('admin::modules.communities.index', compact('communities'));
    }

    public function communitiesChange(Request $request)
    {
        $user_id        = $request->f_id;
        $communities_id = $request->l_id;

        $user = User::find($user_id);

        if ($request->parametr == 'true') {
            $user->communities()->attach($communities_id);
        } elseif ($request->parametr == 'false') {
            $user->communities()->detach($communities_id);
        }

        return response()->json('ok');
    }

    public function communityChange(Request $request)
    {
        $user_id      = $request->f_id;
        $community_id = $request->l_id;

        $user      = User::find($user_id);
        $community = Community::find($community_id);

        if ($request->parametr == 'true') {
            $community->user_id = $user_id;
        } elseif ($request->parametr == 'false') {
            $community->user_id = 0;
        }
        $community->save();

        return response()->json('ok');
    }
}
