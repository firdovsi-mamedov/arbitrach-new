<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use  Modules\Main\Entities\Menu;
use Modules\Admin\Http\Requests\MenuCreateRequest;
use Modules\Admin\Http\Requests\MenuUpdateRequest;
use Modules\Post\Entities\Category;

class MenusController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $menus = Menu::orderBy('order')->paginate(4);

        return view('admin::modules.menus.index', compact('menus'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Menu $menu)
    {
        return view('admin::modules.menus.item', compact('menu'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(MenuCreateRequest $request)
    {
        if($request->filled('slug')){
            $slug = $request->input('slug');
        }else{
            $slug = str_slug($request->input('title'));
        }

        Menu::create([
            'title'=>$request->title,
            'order'=>$request->order,
            'slug'=>$slug,
            'active'=>$request->active
        ]);

        return redirect(route('admin.menus.index'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Menu $menu)
    {
        $categoriesCollection = Category::all();
        $categories = parent::dataSome($categoriesCollection, "categories", $menu, 'belongsToMany');

        return view('admin::modules.menus.item', compact('menu','categories'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(MenuUpdateRequest $request,Menu $menu)
    {
        $menu->categories()->sync($request->checked);

        if($request->filled('slug')){
            $slug = $request->input('slug');
        }else{
            $slug = str_slug($request->input('title'));
        }

        $menu->update([
            'title'=>$request->title,
            'order'=>$request->order,
            'slug'=>$slug,
            'active'=>$request->active
        ]);

        return redirect(route('admin.menus.index'));
    }

    public function destroy(Request $request)
    {
        $id = intval($request->id);

        if ($id == 0) {
            return redirect()->back()->withErrors('Ой');
        }

        $menu = Menu::find($id);
        $menu->categories()->sync([]);
        $menu->delete();

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $search_string = '%'.$request->get('search').'%';

        $menus = Menu::where('title', 'like', $search_string)
            ->orderByDesc('id')
            ->paginate(10);

        return view('admin::modules.menus.index', compact('menus'));
    }
}
