<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Admin\Http\Requests\CreateTagRequest;
use Modules\Admin\Http\Requests\UpdateTagRequest;
use Modules\Post\Entities\Tag;
use Modules\Users\Entities\Community;
use Modules\Users\Entities\User;

class TagsController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tags = Tag::orderBy('order')->paginate(20);

        return view('admin::modules.tags.index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     * @param Tag $tag
     * @return Response
     */
    public function create(Tag $tag)
    {
        return view('admin::modules.tags.item', compact('tag'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  CreateTagRequest $request
     * @return Response
     */
    public function store(CreateTagRequest $request)
    {
        Tag::create($request->all());

        return redirect(route('admin.tags.index'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param Tag $tag
     * @return Response
     */
    public function edit(Tag $tag)
    {
        $usersCollection = User::all();
        $users           = parent::dataSome($usersCollection, "users", $tag, 'belongsToMany');
        $tagsTagUrl      = '/admin/TagCheckbox/change';

        return view('admin::modules.tags.item', compact('tag', 'users'), [
            'tagsTagUrl' => $tagsTagUrl
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param  UpdateTagRequest $request
     * @param $tag Tag
     * @return Response
     */
    public function update(UpdateTagRequest $request, Tag $tag)
    {
        if ($request->filled('slug')) {
            $slug = $request->input('slug');
        } else {
            $slug = str_slug($request->input('name'));
        }

        $tag->update([
            'name'   => $request->input('name'),
            'slug'   => $slug,
            'order'  => $request->input('order'),
            'active' => $request->input('active')
        ]);

        return redirect()->route('admin.tags.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param $tag Tag
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Tag $tag)
    {
        $tag->delete();

        return back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $search_string = '%'.$request->get('search').'%';

        $tags = Tag::where('name', 'like', $search_string)
            ->orderByDesc('id')
            ->paginate(10);

        return view('admin::modules.tags.index', compact('tags'));
    }

    public function tagsChange(Request $request)
    {
        $user_id = $request->f_id;
        $tag_id  = $request->l_id;

        $user = User::find($user_id);

        if ($request->parametr == 'true') {
            $user->tags()->attach($tag_id);
        } elseif ($request->parametr == 'false') {
            $user->tags()->detach($tag_id);
        }

        return response()->json('ok');
    }

    public function tagsTagChange(Request $request)
    {
        $tag_id  = $request->f_id;
        $user_id = $request->l_id;

        $tag = Tag::find($tag_id);

        if ($request->parametr == 'true') {
            $tag->users()->attach($user_id);
        } elseif ($request->parametr == 'false') {
            $tag->users()->detach($user_id);
        }

        return response()->json('ok');
    }

    public function communityChangeTagsCheck(Request $request)
    {
        $community_id = $request->f_id;
        $tag_id       = $request->l_id;

        $community = Community::find($community_id);

        if ($request->parametr == 'true') {
            $community->tags()->attach($tag_id);
        } elseif ($request->parametr == 'false') {
            $community->tags()->detach($tag_id);
        }

        return response()->json('ok');
    }

}
