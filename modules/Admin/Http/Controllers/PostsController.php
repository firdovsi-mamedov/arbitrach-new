<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Admin\Http\Requests\UpdatePostRequest;
use Modules\Post\Entities\Category;
use Modules\Users\Entities\Community;
use Modules\Post\Entities\Tag;
use Modules\Post\Entities\Post;
use Modules\Users\Entities\User;

class PostsController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $posts = Post::orderBy('category_id')->paginate(4);

        return view('admin::modules.posts.index', compact('posts'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Post $post)
    {
        $categoriesCollection = Category::all();
        $categories = parent::dataSome($categoriesCollection, "posts", $post, 'hasMany', 'category_id');

        return view('admin::modules.posts.item', compact('post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(UpdatePostRequest $request, Post $post)
    {
        if ($request->has('unchecked')) {
            $checked = 0;
        } else {
            $checked = $request->input('checked',0);
        }

        $my      = $request->input('my', 0);
        $slug    = $request->input('slug', str_slug($request->input('name')));

        $post->update([
            'title'       => $request->title,
            'slug'        => $slug,
            'draft'       => $request->draft,
            'category_id' => $checked,
            'my'          => $my
        ]);

        return redirect(route('admin.posts.index'));
    }

    public function destroy(Request $request)
    {
        $id = intval($request->id);

        if ($id == 0) {
            return redirect()->back()->withErrors('Ой');
        }

        $post = Post::find($id);
        $post->delete();

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $search_string = '%'.$request->get('search').'%';

        $posts = Post::where('title', 'like', $search_string)
            ->orderByDesc('id')
            ->paginate(10);

        return view('admin::modules.posts.index', compact('posts'));
    }

    public function postsChange(Request $request)
    {
        $user_id = $request->f_id;
        $post_id = $request->l_id;

        $user = User::find($user_id);
        $post = Post::find($post_id);

        if ($request->parametr == 'true') {
            $post->user_id = $user_id;
        } elseif ($request->parametr == 'false') {
            $post->user_id = 0;
        }
        $post->save();

        return response()->json('ok');
    }

public function communityChangePostsCheck(Request $request)
{
    $community_id = $request->f_id;
    $post_id = $request->l_id;

    $community = Community::find($community_id);
    $post = Post::find($post_id);

    if ($request->parametr == 'true') {
        $post->community_id = $community_id;
    } elseif ($request->parametr == 'false') {
        $post->community_id = 0;
    }
    $post->save();

    return response()->json('ok');
  }
}
