<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Admin\Http\Requests\UserCreateRequest;
use Modules\Admin\Http\Requests\UserUpdateRequest;
use Modules\Post\Entities\Post;
use Modules\Post\Entities\Tag;
use Modules\Users\Entities\Community;
use Modules\Users\Entities\Role;
use Modules\Users\Entities\User;

class UsersController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $users = User::orderByDesc('id')->paginate(10);

        return view('admin::modules.users.index', compact('users'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $search_string = '%'.$request->get('search').'%';

        $users = User::where('name', 'like', $search_string)
            ->orWhere('email', 'like', $search_string)
            ->orderByDesc('id')
            ->paginate(10);

        return view('admin::modules.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     * @param User $user
     * @return Response
     */
    public function create(User $user)
    {
        $roles = Role::all();

        return view('admin::modules.users.item', compact('user', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  UserCreateRequest $request
     * @return Response
     */
    public function store(UserCreateRequest $request)
    {
        User::create($request->all());

        return redirect(route('admin.users.index'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param $user User
     * @return Response
     */
    public function edit(User $user)
    {
        $tagsCollection = Tag::all();
        $roles          = Role::all();
        $tags           = parent::dataSome($tagsCollection, "subscribedTags", $user, 'belongsToMany');

        $communitiesCollection = Community::all();
        $communities           = parent::dataSome($communitiesCollection, "communities", $user, 'belongsToMany');
        $community             = parent::dataSome($communitiesCollection, "community", $user, 'hasMany', 'user_id');

        $postsCollection = Post::all();
        $posts           = parent::dataSome($postsCollection, "posts", $user, 'hasMany', 'user_id');

        $tagsUrl        = '/admin/tagsCheckbox/change';
        $communitiesUrl = '/admin/communitiesCheckbox/change';
        $postsUrl       = '/admin/postsCheckbox/change';
        $communityUrl   = '/admin/categoryCheckbox/change';

        return view('admin::modules.users.item', compact('user', 'tags', 'communities', 'posts', 'community', 'roles'),
            [
                'tagsUrl'        => $tagsUrl,
                'communitiesUrl' => $communitiesUrl,
                'postsUrl'       => $postsUrl,
                'communityUrl'   => $communityUrl
            ]);
    }

    /**
     * Update the specified resource in storage.
     * @param  UserUpdateRequest $request
     * @param $user User
     * @return Response
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        $user->update($request->all());

        return redirect()->route('admin.users.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param $user User
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        $user->delete();

        return back();
    }

    public function communityChangeUsersCheck(Request $request)
    {
        $community_id = $request->f_id;
        $user_id      = $request->l_id;

        $community = Community::find($community_id);

        if ($request->parametr == 'true') {
            $community->users()->attach($user_id);
        } elseif ($request->parametr == 'false') {
            $community->users()->detach($user_id);
        }

        return response()->json('ok');
    }
}
