<?php

namespace Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Users\Entities\User;

class UserCreateRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                             => 'required|string',
            'email'                            => 'required|email|unique:users',
            'rating'                           => 'required|integer',
            'show_posts_with_rating_higher'    => 'required|integer',
            'posts_per_page'                   => 'required|integer',
            'show_comments_with_rating_higher' => 'required|integer',
            'collapse_comment_after_level'     => 'required|integer',
            'password'                         => 'string|min:6',
            'role_id'                          => 'required|integer|exists:roles,id'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $fields = (new User)->getBoolFields();

        foreach ($fields as $field) {
            $this->request->set($field, $this->request->has($field) && $this->request->get($field) === 'on');
        }
    }
}
