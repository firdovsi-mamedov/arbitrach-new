<?php

namespace Modules\Post\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use Modules\Post\Entities\Tag;
use Modules\Users\Entities\User;

class TagsSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        Model::unguard();
        Tag::truncate();

        for ($i = 0; $i < 30; $i++) {
            $tag = Tag::create([
                'name' => $faker->word,
                'slug' => $faker->unique()->slug
            ]);
            $tag->users()->attach(rand(1, 6));
            $tag->users()->attach(rand(1, 6));
            $tag->users()->attach(rand(1, 6));
        }
    }
}
