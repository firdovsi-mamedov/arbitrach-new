<?php

namespace Modules\Post\Database\Seeders;

use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Post\Entities\Category;
use Modules\Post\Entities\Chunk;
use Modules\Post\Entities\Post;
use Modules\Post\Entities\Tag;

class PostTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Generator $faker)
    {
        Model::unguard();
        Post::truncate();

        $categories = Category::all();
        $tags       = Tag::all();

        $content = \File::get(public_path('image/photo.png'));
        $path    = 'public/posts/default_image.png';

        \Storage::put($path, $content);

        foreach (range(1, 20) as $i) {
            $post = Post::create([
                'title'       => $faker->sentence,
                'slug'        => $faker->unique()->slug,
                'category_id' => $categories->random()->id,
                'user_id'     => rand(1, 6),
                'my'          => true,
            ]);

            $post->chunks()->create([
                'content' => $path,
                'type'    => Chunk::$TYPE_IMAGE,
                'order'   => 1
            ]);

            $post->chunks()->create([
                'content' => 'For more straightforward sizing in CSS, we switch the global box-sizing value from content-box to border-box. This ensures padding does not affect the',
                'type'    => Chunk::$TYPE_TEXT,
                'order'   => 2
            ]);

            $post->chunks()->create([
                'content' => 'final computed width of an element, but it can cause problems with some third party software like Google Maps and Google Custom Search Engine. For more straightforward sizing in CSS, we switch the global box-sizing value from content-box to border-box. This ensures padding does not affect the final computed width of an element, but it can cause problems with some third party software like Google Maps and Google Custom Search Engine.',
                'type'    => Chunk::$TYPE_TEXT,
                'order'   => 3
            ]);

            $post->tags()->saveMany($tags->take(rand(2, 5)));
        }
    }
}
