<?php

namespace Modules\Post\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Modules\Main\Entities\Menu;
use Modules\Post\Entities\Category;

class CategoryTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        Category::truncate();

        $categories = collect([
            'Юмор',
            'Кейсы',
            'Статьи',
            'Другое'
        ]);

        foreach ($categories as $i => $category) {
            Category::create([
                'name'  => $category,
                'slug'  => Str::slug($category),
                'order' => ++$i
            ]);
        }
    }
}
