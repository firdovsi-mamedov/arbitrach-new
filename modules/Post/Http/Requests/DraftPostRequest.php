<?php

namespace Modules\Post\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class DraftPostRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'            => 'sometimes|max:255',
            'category_id'      => 'sometimes|integer|nullable',
            'my'               => 'sometimes|boolean',
            'draft'            => 'sometimes|boolean',
            'chunks'           => 'sometimes|array',
            'chunks.*.content' => 'required|max:2000',
            'chunks.*.type'    => 'sometimes|max:255',
        ];
    }

    protected function prepareForValidation()
    {
        if (is_null($this->get('my'))) {
            $this->merge(['my' => false]);
        }

        if (is_null($this->get('erotic'))) {
            $this->merge(['erotic' => false]);
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'chunks.*.content.required' => 'Введите пожалуйста содержимое поста.',
        ];
    }
}
