<?php

namespace Modules\Post\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class StorePostRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'            => 'required|string|max:255',
            'slug'             => 'required|string|max:255|unique:posts,slug',
            'category_id'      => 'required|integer|exists:categories,id',
            'my'               => 'sometimes|boolean',
            'draft'            => 'sometimes|boolean',
            'chunks'           => 'required|array',
            'chunks.*.content' => 'required|string|max:2000',
            'chunks.*.type'    => 'required|string|max:255',
        ];
    }

    protected function prepareForValidation()
    {
        if (! $this->has('slug') || $this->draft) {
            $this->merge(['slug' => Str::slug($this->title)]);
        }

        if (is_null($this->get('my'))) {
            $this->merge(['my' => false]);
        }

        if (is_null($this->get('erotic'))) {
            $this->merge(['erotic' => false]);
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'category_id.required'      => 'Выберите пожалуйста категорию.',
            'category_id.exists'        => 'Выберите пожалуйста категорию.',
            'title.required'            => 'Введите пожалуйста название поста.',
            'chunks.required'           => 'Введите пожалуйста содержимое поста.',
            'chunks.array'              => 'Введите пожалуйста содержимое поста.',
            'chunks.*.content.required' => 'Введите пожалуйста содержимое поста.',
        ];
    }
}
