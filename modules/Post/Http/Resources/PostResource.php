<?php

namespace Modules\Post\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Modules\Users\Http\Resources\CommunityResource;
use Modules\Users\Http\Resources\UserResource;

class PostResource extends Resource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $edit = $this->additional['edit'] ?? false;
        $auth = \Auth::check();

        return [
            'id'                 => $this->id,
            'title'              => $this->title,
            'slug'               => $this->slug,
            'category_id'        => $this->category_id,
            'community_id'       => $this->community_id,
            'my'                 => $this->my,
            'erotic'             => $this->erotic,
            'draft'              => $this->draft,
            'viewed'             => $this->when(! $edit && $auth, function () {
                return $this->resource->isViewed(\Auth::id());
            }),
            'viewed_posts_state' => $this->when(! $edit && $auth, function () {
                return \Auth::user()->viewed_posts_state;
            }),
            'erotic_posts_state' => $this->when(! $edit && $auth, function () {
                return \Auth::user()->erotic_posts_state;
            }),
            'rating'             => $this->when(! $edit, $this->rating),
            'comments_count'     => $this->when(! $edit, $this->comments()->count()),
            'editable'           => $this->when(! $edit && $auth, \Auth::id() === $this->user_id),
            'saveable'           => $this->when(! $edit && $auth, function () {
                return \Auth::user()->savedPosts()->where('saveable_id', $this->id)->exists();
            }),
            'show'               => $this->when(! $edit, function () {
                return $this->slug ? route('posts.show', $this->slug) : null;
            }),
            'edit'               => $this->when(! $edit, function () {
                return $this->slug ? route('posts.edit', $this->slug) : null;
            }),
            'ago'                => $this->when(! $edit, function () {
                return \Jenssegers\Date\Date::parse($this->created_at)->ago();
            }),
            'chunks'             => $this->whenLoaded('chunks', function () {
                return ChunkResource::collection($this->chunks);
            }),
            'user'               => $this->whenLoaded('user', function () {
                return UserResource::make($this->user);
            }),
            'tags'               => $this->whenLoaded('tags', function () {
                return TagResource::collection($this->tags);
            }),
            'community'          => $this->whenLoaded('community', function () {
                return CommunityResource::make($this->community);
            })
        ];
    }
}
