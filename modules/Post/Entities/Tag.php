<?php

namespace Modules\Post\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Modules\Users\Entities\Community;
use Modules\Users\Entities\User;

/**
 * Modules\Post\Entities\Tag
 *
 * @property int $id
 * @property string $name
 * @property int $order
 * @property int $active
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Post\Entities\Post[] $posts
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Users\Entities\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Tag whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Tag whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Tag whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Tag whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Tag whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Tag whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $slug
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Users\Entities\Community[] $communities
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Tag prepared()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Tag whereSlug($value)
 */
class Tag extends Model
{

    protected $guarded = ['id'];

    protected $appends = ['show'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function getShowAttribute()
    {
        return route('tags.show', $this->slug);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts()
    {
        return $this->belongsToMany(Post::class, 'post_tag');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class);

    }

    /**
     * @param Builder $query
     */
    public function scopePrepared($query)
    {
        $query->where('active', true)->orderBy('order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function communities()
    {
        return $this->belongsToMany(Community::class);
    }
}
