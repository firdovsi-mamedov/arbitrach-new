<?php

namespace Modules\Post\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Modules\Post\Scopes\PostScope;
use Modules\Users\Entities\Community;
use Modules\Users\Entities\User;

/**
 * Modules\Post\Entities\Post
 *
 * @property int $id
 * @property string|null $slug
 * @property string|null $title
 * @property int|null $category_id
 * @property int|null $community_id
 * @property int $user_id
 * @property int $rating
 * @property int $my
 * @property int $draft
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Modules\Post\Entities\Category|null $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Post\Entities\Chunk[] $chunks
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Post\Entities\Comment[] $comments
 * @property-read \Modules\Users\Entities\Community|null $community
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Post\Entities\Like[] $likes
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Post\Entities\Comment[] $rootComments
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Post\Entities\Tag[] $tags
 * @property-read \Modules\Users\Entities\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Post prepared()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Post whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Post whereCommunityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Post whereDraft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Post whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Post whereMy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Post wherePublish($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Post whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Post whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Post whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Post whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Post whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Post withoutIgnoredTags()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Post withoutIgnoredUsers()
 * @mixin \Eloquent
 */
class Post extends Model
{

    protected $fillable = [
        'slug',
        'title',
        'category_id',
        'community_id',
        'user_id',
        'rating',
        'my',
        'draft',
        'erotic'
    ];

    /**
     * boot the model scopes
     */
    protected static function boot()
    {
        parent::boot();

        if (! \Route::currentRouteNamed('api.likes.*') &&
            ! \Route::currentRouteNamed('api.saves.*') &&
            ! \Route::currentRouteNamed('admin.*')
        ) {
            static::addGlobalScope(new PostScope());
        }
    }

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function community()
    {
        return $this->belongsTo(Community::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'post_tag');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * s*/
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function chunks()
    {
        return $this->hasMany(Chunk::class)->orderBy('order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rootComments()
    {
        return $this->hasMany(Comment::class)->where('parent_id', null);
    }

    /**
     * @param $query Builder
     */
    public function scopePrepared($query)
    {
        /** @var Builder $query */
        $query->with(['chunks', 'tags', 'user', 'community'])
            ->withCount('comments')
            ->where('draft', false);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeWithoutIgnoredUsers($query)
    {
        $ids = \Auth::user()->subscribedUsers()
            ->where('ignored', true)
            ->get()
            ->pluck('id');

        return $query->whereNotIn('user_id', $ids);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeByUserRating($query)
    {
        return $query->where('rating', '>=', \Auth::user()->show_posts_with_rating_higher ?? 0);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeWithoutIgnoredTags($query)
    {
        $ids = \Auth::user()->subscribedTags()
            ->where('ignored', true)
            ->get()
            ->pluck('id');

        return $query->whereDoesntHave('tags', function ($q) use ($ids) {
            $q->whereIn('tag_id', $ids);
        });
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeWithoutIgnoredCommunities($query)
    {
        $ids = \Auth::user()->communities()
            ->where('ignored', true)
            ->get()
            ->pluck('id');

        return $query->whereNotIn('community_id', $ids);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function userViews()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * @param int $user_id
     * @return bool
     */
    public function isViewed(int $user_id): bool
    {
        return $this->userViews()->where('user_id', $user_id)->exists();
    }
}
