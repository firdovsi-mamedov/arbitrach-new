<?php

namespace Modules\Post\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Modules\Post\Entities\Like
 *
 * @property int $id
 * @property int $likeable_id
 * @property string $likeable_type
 * @property int $user_id
 * @property int $vote
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $likeable
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Like whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Like whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Like whereLikeableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Like whereLikeableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Like whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Like whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Like whereVote($value)
 * @mixin \Eloquent
 * @property-read \Modules\Post\Entities\Comment $comments
 * @property-read \Modules\Post\Entities\Post $posts
 */
class Like extends Model
{

    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function likeable()
    {
        return $this->morphTo();
    }

    /**
     * @return mixed
     */
    public function post()
    {
        return $this->belongsTo(Post::class, 'likeable_id');
    }

    public function preparedPost()
    {
        return $this->post()->prepared();
    }

    /**
     * @return mixed
     */
    public function comment()
    {
        return $this->belongsTo(Comment::class, 'likeable_id');
    }
}
