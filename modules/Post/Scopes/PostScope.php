<?php

namespace Modules\Post\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class PostScope implements Scope
{

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        if ($user = \Auth::user()) {
            $builder
                ->withoutIgnoredUsers()
                ->withoutIgnoredTags()
                ->byUserRating()
                ->withoutIgnoredCommunities();
        }
    }
}