<?php

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Post\Http\Controllers'], function () {

    Route::group(['prefix' => 'posts'], function () {
        Route::group(['middleware' => 'auth'], function () {
            Route::get('create', 'PostController@create')->name('posts.create');
            Route::get('{post}/edit', 'PostController@edit')->name('posts.edit');
            Route::put('{post}', 'PostController@update')->name('posts.update');
            Route::post('/', 'PostController@store')->name('posts.store');
            Route::post('draft', 'PostController@draft')->name('posts.draft');
            Route::post('store-image', 'PostController@storeImage');
            Route::delete('chunks/{chunk}', 'PostController@deleteChunk');

        });

        Route::get('{post}', 'PostController@show')->name('posts.show');
    });

    Route::group(['prefix' => 'tags', 'as' => 'tags.'], function () {
        Route::get('/', 'TagsController@index')->name('index');
        Route::post('/search', 'TagsController@search')->name('search');
        Route::get('/{tag}', 'TagsController@show')->name('show');
    });

});
