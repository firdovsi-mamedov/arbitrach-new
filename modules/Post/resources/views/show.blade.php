@extends('main::layouts.master')

@section('content')
    <post style="margin-top: 15px" :data="{{ json_encode($post) }}"></post>

    <a name="comments"></a>

    <comment-list id="{{ $post['id'] }}" api-url="{{ route('api.comments.index') }}"></comment-list>

@endsection