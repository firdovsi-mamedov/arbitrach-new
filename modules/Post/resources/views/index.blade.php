@extends('main::layouts.master')

@section('content')
    <nav class="navbar navbar-expand-lg">
        <a class="nav-link first-link {{ $category->id ? '' : 'active' }}"
           href="{{ route('index', $menu->slug) }}" title="Все посты">Все посты <span
                    class="sr-only">(current)</span></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav1"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav1">
            <ul class="navbar-nav">
                @foreach($categories as $item)
                    <li class="nav-item">
                        <a class="nav-link {{ $category->id === $item->id ? 'active' : '' }}"
                           href="{{ route('index', [$menu->slug, $item->slug]) }}"
                           title="{{ $item->name }}">
                            {{ $item->name }}
                        </a>
                    </li>
                @endforeach
            </ul>

            <date-filter
                    style="width: 50%"
                    @if(Auth::user())
                    night="{{ Auth::user()->is_night_mode }}"
                    @else
                    night="false"
                    @endif
            ></date-filter>
        </div>
    </nav>

    <post-list
            menu="{{ $menu->slug }}"
            category="{{ $category->id }}"
            api-url="{{ route('api.posts.index') }}"
    ></post-list>

@stop