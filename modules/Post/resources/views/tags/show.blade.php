@extends('main::layouts.master')

@section('content')
    <post-list api-url="{{ route('api.tags.posts', [$tag->slug]) }}">
        <tags-filter slot="filter" style="margin: 15px 0"></tags-filter>
    </post-list>
@stop