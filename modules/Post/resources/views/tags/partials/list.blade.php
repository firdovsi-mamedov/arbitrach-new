<div class="post-block" style="margin-top: 15px">
    <div>
        Популярные теги
    </div>
    <div class="container-content-heshtag" style="margin-top: 15px">
        @forelse($tags as $tag)
            <div>
                <a style="text-decoration: none" href="{{ route('tags.show', $tag->slug) }}">{{ $tag->name.' '.$tag->posts_count }}</a>
            </div>
        @empty
            <div>
                Тэги не найдены
            </div>
        @endforelse
    </div>
</div>