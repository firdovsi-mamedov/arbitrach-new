@extends('main::layouts.master')

@section('content')

    <tags-search style="margin-top: 15px"></tags-search>

    <div id="tags-list">
        @include('post::tags.partials.list')
    </div>
@stop