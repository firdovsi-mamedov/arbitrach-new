@extends('main::layouts.master-without-right')

@section('content')
    <div id="app">
        <section class="main-container">

            <post-editor :initpost="{{ $post }}" :categories="{{ $categories }}"></post-editor>

            <div class="container-right">

                <div class="personal-banner i-mt-15">
                    <img src="{{ asset('image/banner1.png') }}" alt="banner">
                </div>

                <div class="personal-tags">
                    <a href="#">Android</a>
                    <a href="#">Мобильная версия</a>
                    <a href="#">Реклама</a>
                    <a href="#">Бан</a>
                    <a href="#">Вакансия</a>
                    <a href="#">Дзен</a>
                    <a href="#">VK</a>
                    <a href="#">Android</a>
                    <a href="#">Android</a>
                    <a href="#">Android</a>
                    <a href="#">Android</a>
                    <a href="#">Android</a>
                    <a href="#">Android</a>
                </div>
            </div>
        </section>
    </div>
@stop