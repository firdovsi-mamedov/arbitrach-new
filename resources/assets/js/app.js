require('./bootstrap');
require('./scripts');

window.Vue = require('vue');




import Autocomplete from 'v-autocomplete'
import VueYouTubeEmbed from 'vue-youtube-embed'
import VModal from 'vue-js-modal'
import VueScrollTo from 'vue-scrollto';
import BaseKeyNavigation from "./mixins/BaseKeyNavigation";

Vue.use(Autocomplete);
Vue.use(VueYouTubeEmbed);
Vue.use(VModal);
Vue.use(VueScrollTo);
Vue.use(infiniteScroll)

//posts
Vue.component('post-editor', require('./components/posts/PostEditor'));
Vue.component('content-loader', require('./components/posts/ContentLoader'));
Vue.component('date-filter', require('./components/posts/DateFilter'));
Vue.component('post-filter', require('./components/posts/PostFilter'));
Vue.component('post-list', require('./components/posts/List'));
Vue.component('post', require('./components/posts/Post'));
//follows
Vue.component('user-follow-list', require('./components/follows/UserFollowList'));
Vue.component('tag-follow-list', require('./components/follows/TagFollowList'));
Vue.component('community-follow-list', require('./components/follows/CommunityFollowList'));
//ignores
Vue.component('user-ignore-list', require('./components/ignores/UserIgnoreList'));
Vue.component('tag-ignore-list', require('./components/ignores/TagIgnoreList'));
Vue.component('community-ignore-list', require('./components/ignores/CommunityIgnoreList'));
//communities
Vue.component('subscribe-community-button', require('./components/communities/SubscribeCommunityButton'));
Vue.component('community-filter', require('./components/communities/CommunityFilter'));
Vue.component('community-post-filter', require('./components/communities/CommunityPostFilter'));
Vue.component('store-community', require('./components/communities/StoreCommunity'));
//tags
Vue.component('tags-filter', require('./components/tags/TagsFilter'));
Vue.component('tags-search', require('./components/tags/TagsSearch'));
//likes
Vue.component('post-like', require('./components/likes/PostLike'));
Vue.component('comment-like', require('./components/likes/CommentLike'));
Vue.component('likes-list', require('./components/likes/List'));
//comments
Vue.component('comment-list', require('./components/comments/CommentList'));
Vue.component('user-comment-list', require('./components/comments/UserCommentList'));
Vue.component('comment', require('./components/comments/Comment'));
//saves
Vue.component('comment-save', require('./components/saves/CommentSave'));
Vue.component('post-save', require('./components/saves/PostSave'));
//answers
Vue.component('answers-list', require('./components/answers/AnswersList'));
//users
Vue.component('user-settings', require('./components/users/Settings'));
Vue.component('user-actions', require('./components/users/Actions'));
Vue.component('viewed-posts', require('./components/users/ViewedPosts'));
Vue.component('erotic-posts', require('./components/users/EroticPosts'));
//notes
Vue.component('note-input', require('./components/notes/Input'));
Vue.component('note-list', require('./components/notes/List'));
// search
Vue.component('search', require('./components/Search'));


Vue.component('auth', require('./components/auth/Auth'));
Vue.component('login', require('./components/auth/Login'));
Vue.component('register', require('./components/auth/Register'));
Vue.component('forgot', require('./components/auth/Forgot'));

const app = new Vue({
    el: '#app',
    mixins: [BaseKeyNavigation]
});
