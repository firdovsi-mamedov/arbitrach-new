import {EventBus} from "../services/EventBus";

export default {
    created() {
        document.addEventListener('keyup', this.defineListEvent);
    },

    methods: {
        defineListEvent(e) {
            if (e.target.tagName === 'TEXTAREA' ||
                e.target.tagName === 'INPUT' ||
                e.target.tagName === 'DIV') {
                return;
            }

            // A
            if (e.which === 65) {
                EventBus.prevPost();
            }

            // D
            if (e.which === 68) {
                EventBus.nextPost();
            }

            // X
            if (e.which === 88) {
                EventBus.topPost();
            }

            // F
            if (e.which === 70) {
                EventBus.toggleCollapse();
            }
        }
    }
}