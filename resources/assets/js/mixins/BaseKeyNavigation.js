export default {
    created() {
        document.addEventListener('keyup', this.defineKeyEvent);
    },

    methods: {
        defineKeyEvent(e) {
            if (e.target.tagName === 'TEXTAREA' ||
                e.target.tagName === 'INPUT' ||
                e.target.tagName === 'DIV') {
                return;
            }

            // Z
            if (e.which === 90) {
                window.scrollBy(0, -100)
            }

            // C
            if (e.which === 67) {
                window.scrollBy(0, 100)
            }
        }
    }
}