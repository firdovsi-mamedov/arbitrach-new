import ListKeyNavigation from "./ListKeyNavigation";
import infiniteScroll from 'vue-infinite-scroll'

export default {
    mixins: [ListKeyNavigation],

    directives: {infiniteScroll},

    data() {
        return {
            api: '',
            items: [],
            links: [],
            meta: [],
            isProceed: false,
            filter: {},
            params: {}
        }
    },

    created() {
        this.getData();
        this.$on('updateFilter', this.updateFilter);
    },

    methods: {
        getData() {
            axios.post(this.api, Object.assign({filter: this.filter}, this.params))
                .then(res => {
                    this.items = res.data.data;
                    this.links = res.data.links;
                    this.meta = res.data.meta;
                })
        },

        loadMore() {
            if (this.isProceed || !this.links.next) return;

            this.isProceed = true;

            axios.post(this.links.next, Object.assign({filter: this.filter}, this.params))
                .then(res => {
                    this.items = _.concat(this.items, res.data.data);
                    this.links = res.data.links;
                    this.meta = res.data.meta;
                    this.isProceed = false;
                })
        },

        updateFilter(filter) {
            this.filter = Object.assign(filter, this.filter);
            this.getData();
        }
    }
}