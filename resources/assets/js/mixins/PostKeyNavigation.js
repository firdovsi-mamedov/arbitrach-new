import {EventBus} from "../services/EventBus";

export default {
    created() {
        document.addEventListener('keyup', this.definePostEvent);
    },

    methods: {
        definePostEvent(e) {
            if (e.target.tagName === 'TEXTAREA' ||
                e.target.tagName === 'INPUT' ||
                e.target.tagName === 'DIV') {
                return;
            }
            
            // W
            if (e.which === 87) {
                EventBus.addPostLike();
            }

            // S
            if (e.which === 83) {
                EventBus.delPostLike();
            }

            // E
            if (e.which === 69) {
                EventBus.openComments();
            }

            // Q
            if (e.which === 81) {
                EventBus.closeComments();
            }
        }
    }
}