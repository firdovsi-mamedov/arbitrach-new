import Noty from 'noty'

Noty.overrideDefaults({
    layout: 'topCenter',
    theme: 'sunset',
    closeWith: ['click'],
    timeout: 1500,
});

export default {
    success(text) {
        new Noty({
            type: 'success',
            text: text
        }).show();
    },

    error(text) {
        new Noty({
            type: 'error',
            text: text
        }).show();
    }
}